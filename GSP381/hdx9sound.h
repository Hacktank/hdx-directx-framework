#pragma once

#include <fmod.hpp>
#include <fmod_errors.h>

#pragma comment(lib,"fmodex_vc.lib")

#include <functional>
#include <unordered_map>

#define HDX_SOUNDMAN HDX9SoundManager::instance()

#define FMODERR_FATAL(errormsg) do {MessageBox(0,errormsg"\n\nApplication will now exit","Error",0); PostQuitMessage(0);} while(0)
#define FMODERR_FATAL_CHECK(hr,errormsg) do {if(hr!=FMOD_OK) FMODERR_FATAL(errormsg);} while(0)

enum HDX9SOUNDTYPE {
	HDX9ST_FAF = 0,
	HDX9ST_STREAM,
};

class HDX9Sound {
	friend class HDX9SoundManager;

public:
	~HDX9Sound();

	FMOD::Sound* getFMODSound();
	const FMOD_CREATESOUNDEXINFO* getFMODSoundEXInfo() const;
	const FMOD_MODE getFMODMode() const;

	const std::string getName() const;

	int release();

private:
	FMOD::Sound *mfmod_sound;
	FMOD_CREATESOUNDEXINFO mfmod_exinfo;
	FMOD_MODE mfmod_mode;

	HDX9SOUNDTYPE mtype;
	std::string mname;
	int musers;

	HDX9Sound(const std::string &name,HDX9SOUNDTYPE type,FMOD_MODE mfmodmode);
};

struct HDX9SoundChannelEffectBase {
	int mcbid_onupdate;
	FMOD::Channel *mfmod_channel;
	HDX9Sound *msound;

	HDX9SoundChannelEffectBase();
	virtual ~HDX9SoundChannelEffectBase();

	virtual bool isComplete() = 0;

	void setFunctionOnUpdate(std::function<void(float)> &&func);
};

class HDX9SoundManager {
	friend class HDX9Sound;

public:
	static HDX9SoundManager* instance() { static HDX9SoundManager *gnew = new HDX9SoundManager(); return gnew; }
	~HDX9SoundManager();

	FMOD_RESULT initializeFMOD(int maxchannels);

	FMOD::System* getFMODSystem();

	void update();

	HDX9Sound* loadSoundFAF(const std::string &name,FMOD_MODE fmmode);
	HDX9Sound* getSoundFAF(const std::string &name);

	HDX9Sound* loadSoundStream(const std::string &name,FMOD_MODE fmmode);
	HDX9Sound* getSoundStream(const std::string &name);

	FMOD::ChannelGroup* createChannelGroup(const std::string &name);
	FMOD::ChannelGroup* getChannelGroup(const std::string &name);
	void destroyChannelGroup(const std::string &name);

	FMOD::Channel* playSoundChannelGroup(HDX9Sound *sound,const std::string &cgroup,bool paused = false,int numloops = 0);
	FMOD::Channel* playSoundChannelGroup(HDX9Sound *sound,FMOD::ChannelGroup *cgroup,bool paused = false,int numloops = 0);
	FMOD::Channel* playSoundChannelGroup(FMOD::Sound *sound,const std::string &cgroup,bool paused = false,int numloops = 0);
	FMOD::Channel* playSoundChannelGroup(FMOD::Sound *sound,FMOD::ChannelGroup *cgroup,bool paused = false,int numloops = 0);

	void channelEffectFadeVolume(FMOD::Channel *channel,float time,float startvolume,float endvolume);

private:
	FMOD::System *mfmod_system;

	std::unordered_map<std::string,HDX9Sound*> mfmod_sounds_faf;
	std::unordered_map<std::string,HDX9Sound*> mfmod_sounds_stream;
	std::unordered_map<std::string,FMOD::ChannelGroup*> mfmod_channelgroups;

	HDX9SoundManager();

	void _onSoundCreate(HDX9Sound *snd);
	void _onSoundDestroy(HDX9Sound *snd);
};
