#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <directxmath.h>

class HDX9IntersectionTests {
public:
	static bool box_point(const D3DXVECTOR3 *boxext,const D3DXVECTOR3 *boxpos,const D3DXVECTOR3 *point);
	static bool box_point(const RECT *box,const POINT *point);

	static bool obb_point(const D3DXVECTOR3 *boxext,const D3DXMATRIX *trans,const D3DXVECTOR3 *point);

	static bool obb_obb(const D3DXVECTOR3 *boxext0,const D3DXMATRIX *trans0,const D3DXVECTOR3 *boxext1,const D3DXMATRIX *trans1);
};
