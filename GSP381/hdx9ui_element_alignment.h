#include "hdx9ui.h"

#define HDXAL_CENTER 0x00000001
#define HDXAL_LEFT 0x00000002
#define HDXAL_RIGHT 0x00000004
#define HDXAL_VCENTER 0x00000008
#define HDXAL_TOP 0x00000010
#define HDXAL_BOTTOM 0x00000020

class HDXUIElement_Alignment : public HDXUIElementBase {
public:
	HDXUIElement_Alignment(const std::string name,
						   HDXUIElementBase *parent,
						   DWORD flags,
						   float dirx,
						   float diry,
						   float posx,
						   float posy,
						   float padding);
	virtual ~HDXUIElement_Alignment();

	D3DXVECTOR2 getDirection() const;
	void getDirection(D3DXVECTOR2 *dir) const;
	void setDirection(const D3DXVECTOR2 *dir);
	void setDirection(const float &x,const float &y);

	D3DXVECTOR2 getCurrentPostion() const;
	void getCurrentPostion(D3DXVECTOR2 *pos) const;
	void setCurrentPostion(const D3DXVECTOR2 *pos);
	void setCurrentPostion(const float &x,const float &y);

	D3DXVECTOR2 getNextPostion(HDXUIElementBase *newelement);
	void getNextPostion(D3DXVECTOR2 *pos,HDXUIElementBase *newelement);

	void jumpDistance(float dst);

	DWORD getFlags() const;
	void setFlags(const DWORD &flags);

	D3DXVECTOR2 getOffset() const;
	void getOffset(D3DXVECTOR2 *pos) const;
	void setOffset(const D3DXVECTOR2 *pos);
	void setOffset(const float &x,const float &y);

	float getPadding() const;
	void setPadding(const float &padding);

protected:
	virtual void _addChild(HDXUIElementBase *child);

private:
	DWORD mflags;
	D3DXVECTOR2 mdir,mcurpos,moffset;
	float mpadding;
};
