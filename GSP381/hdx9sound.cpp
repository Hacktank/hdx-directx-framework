
#include "hdx9sound.h"

#include <windows.h>
#include <algorithm>

#include "hdx9.h"

HDX9Sound::HDX9Sound(const std::string &name,HDX9SOUNDTYPE type,FMOD_MODE fmodmode) {
	mname = name;
	musers = 0;
	mfmod_sound = 0;
	mtype = type;
	mfmod_mode = fmodmode;

	FMOD::System *fms = HDX_SOUNDMAN->getFMODSystem();

	if(fms) {
		FMOD_RESULT hr = FMOD_OK;
		switch(type) {
			case HDX9ST_FAF: {
				hr = fms->createSound(name.c_str(),fmodmode,0,&mfmod_sound);
				break;
			}
			case HDX9ST_STREAM: {
				hr = fms->createStream(name.c_str(),fmodmode,0,&mfmod_sound);
				break;
			}
		}
		if(hr != FMOD_OK) {
			char buff[256];
			sprintf_s(buff,"Failed to load sound \"%s\".",name.c_str());
			MessageBox(0,buff,0,0);
		}
	}

	HDX_SOUNDMAN->_onSoundCreate(this);
}

HDX9Sound::~HDX9Sound() {
	if(mfmod_sound) mfmod_sound->release();

	HDX_SOUNDMAN->_onSoundDestroy(this);
}

FMOD::Sound* HDX9Sound::getFMODSound() {
	return mfmod_sound;
}

const FMOD_CREATESOUNDEXINFO* HDX9Sound::getFMODSoundEXInfo() const {
	return &mfmod_exinfo;
}

const FMOD_MODE HDX9Sound::getFMODMode() const {
	return mfmod_mode;
}

const std::string HDX9Sound::getName() const {
	return mname;
}

int HDX9Sound::release() {
	int ret = --musers;
	if(musers==0) {
		delete this;
	}
	return ret;
}

HDX9SoundChannelEffectBase::HDX9SoundChannelEffectBase() {
	mcbid_onupdate = -1;
	mfmod_channel = 0;
	msound = 0;
}

HDX9SoundChannelEffectBase::~HDX9SoundChannelEffectBase() {
	setFunctionOnUpdate(nullptr);
}

void HDX9SoundChannelEffectBase::setFunctionOnUpdate(std::function<void(float)> &&func) {
	if(mcbid_onupdate >= 0) HDX_MAIN->unregisterFuncUpdate(mcbid_onupdate);
	if(func != nullptr) {
		mcbid_onupdate = HDX_MAIN->registerFuncUpdate([func,this](float dt)->void {
			if(isComplete()) {
				delete this;
			} else {
				func(dt);
			}
		});
	}
}


HDX9SoundManager::HDX9SoundManager() {

}

HDX9SoundManager::~HDX9SoundManager() {

}

#define __ERRinitializeFMOD(hr) if(hr) { FMODERR_FATAL("FMOD initialization failed."); break; }
FMOD_RESULT HDX9SoundManager::initializeFMOD(int maxchannels) {
	unsigned int version;
	int numdrivers;
	FMOD_SPEAKERMODE speakermode;
	FMOD_CAPS caps;
	char buff[256];

	FMOD_RESULT hr;
	do {
		__ERRinitializeFMOD(FMOD::System_Create(&mfmod_system));

		__ERRinitializeFMOD(mfmod_system->getVersion(&version));

		if(version < FMOD_VERSION) {
			sprintf_s(buff,"Error, you are using an old version of FMOD %08x. This program requires %08x.\n\nApplication will now exit.",
					version,
					FMOD_VERSION);
			MessageBox(0,buff,0,0);
			PostQuitMessage(0);
			break;
		}

		__ERRinitializeFMOD(mfmod_system->getNumDrivers(&numdrivers));

		if(numdrivers == 0) {
			__ERRinitializeFMOD(mfmod_system->setOutput(FMOD_OUTPUTTYPE_NOSOUND));
		} else {
			__ERRinitializeFMOD(mfmod_system->getDriverCaps(0,&caps,0,&speakermode));

			__ERRinitializeFMOD(mfmod_system->setSpeakerMode(speakermode));

			if(caps & FMOD_CAPS_HARDWARE_EMULATED) {
				__ERRinitializeFMOD(mfmod_system->setDSPBufferSize(1024,10));
			}

			__ERRinitializeFMOD(mfmod_system->getDriverInfo(0,buff,256,0));

			if(strstr(buff,"SimaTel")) {
				__ERRinitializeFMOD(mfmod_system->setSoftwareFormat(48000,
					FMOD_SOUND_FORMAT_PCMFLOAT,
					0,
					0,
					FMOD_DSP_RESAMPLER_LINEAR));
			}
		}

		hr = mfmod_system->init(maxchannels,FMOD_INIT_NORMAL,0);
		if(hr == FMOD_ERR_OUTPUT_CREATEBUFFER) {
			__ERRinitializeFMOD(mfmod_system->setSpeakerMode(FMOD_SPEAKERMODE_STEREO));
			hr = mfmod_system->init(maxchannels,FMOD_INIT_NORMAL,0);
		}
		__ERRinitializeFMOD(hr);

	} while(0);

	/*
	* Set up sound channels
	*
	**/

	auto snd_group_master = HDX_SOUNDMAN->createChannelGroup("master");
	auto snd_group_gui = HDX_SOUNDMAN->createChannelGroup("gui");
	auto snd_group_music = HDX_SOUNDMAN->createChannelGroup("music");
	auto snd_group_fx = HDX_SOUNDMAN->createChannelGroup("fx");

	snd_group_master->addGroup(snd_group_gui);
	snd_group_master->addGroup(snd_group_music);
	snd_group_master->addGroup(snd_group_fx);

	return hr;
}
#undef __ERRinitializeFMOD

FMOD::System* HDX9SoundManager::getFMODSystem() {
	return mfmod_system;
}

void HDX9SoundManager::update() {
	mfmod_system->update();
}

HDX9Sound* HDX9SoundManager::loadSoundFAF(const std::string &name,FMOD_MODE fmmode) {
	HDX9Sound *cur = getSoundFAF(name);
	if(cur == 0) {
		cur = new HDX9Sound(name,HDX9ST_FAF,fmmode);
	}
	cur->musers++;
	return cur;
}

HDX9Sound* HDX9SoundManager::getSoundFAF(const std::string &name) {
	if(mfmod_sounds_faf.count(name) > 0) {
		return mfmod_sounds_faf[name];
	}
	return 0;
}

HDX9Sound* HDX9SoundManager::loadSoundStream(const std::string &name,FMOD_MODE fmmode) {
	HDX9Sound *cur = getSoundStream(name);
	if(cur == 0) {
		cur = new HDX9Sound(name,HDX9ST_STREAM,fmmode|FMOD_CREATESTREAM);
	}
	cur->musers++;
	return cur;
}

HDX9Sound* HDX9SoundManager::getSoundStream(const std::string &name) {
	if(mfmod_sounds_stream.count(name) > 0) {
		return mfmod_sounds_stream[name];
	}
	return 0;
}

FMOD::ChannelGroup* HDX9SoundManager::createChannelGroup(const std::string &name) {
	FMOD::ChannelGroup *cur = getChannelGroup(name);
	if(cur == 0) {
		mfmod_system->createChannelGroup(name.c_str(),&cur);
	}
	mfmod_channelgroups[name] = cur;
	return cur;
}

FMOD::ChannelGroup* HDX9SoundManager::getChannelGroup(const std::string &name) {
	if(mfmod_channelgroups.count(name) > 0) {
		return mfmod_channelgroups[name];
	}
	return 0;
}

void HDX9SoundManager::destroyChannelGroup(const std::string &name) {
	FMOD::ChannelGroup *cur = getChannelGroup(name);
	if(cur == 0) {
		cur->release();
	}
	mfmod_channelgroups.erase(name);
}

FMOD::Channel* HDX9SoundManager::playSoundChannelGroup(HDX9Sound *sound,const std::string &cgroup,bool paused,int numloops) {
	return playSoundChannelGroup(sound->getFMODSound(),getChannelGroup(cgroup),paused,numloops);
}

FMOD::Channel* HDX9SoundManager::playSoundChannelGroup(HDX9Sound *sound,FMOD::ChannelGroup *cgroup,bool paused,int numloops) {
	return playSoundChannelGroup(sound->getFMODSound(),cgroup,paused,numloops);
}

FMOD::Channel* HDX9SoundManager::playSoundChannelGroup(FMOD::Sound *sound,const std::string &cgroup,bool paused,int numloops) {
	return playSoundChannelGroup(sound,getChannelGroup(cgroup),paused,numloops);
}

FMOD::Channel* HDX9SoundManager::playSoundChannelGroup(FMOD::Sound *sound,FMOD::ChannelGroup *cgroup,bool paused,int numloops) {
	FMOD::Channel *ret;
	FMOD_RESULT hr = mfmod_system->playSound(FMOD_CHANNEL_FREE,sound,true,&ret);
	ret->setChannelGroup(cgroup);
	if(numloops != 0) {
		ret->setLoopCount(numloops);
		ret->setMode(FMOD_LOOP_NORMAL);
		ret->setPosition(0,FMOD_TIMEUNIT_MS); //flush settings changes
	}
	ret->setPaused(paused);
	return ret;
}

void HDX9SoundManager::_onSoundCreate(HDX9Sound *snd) {
	switch(snd->mtype) {
		case HDX9ST_FAF: {
			mfmod_sounds_faf[snd->getName()] = snd;
			break;
		}
		case HDX9ST_STREAM: {
			mfmod_sounds_stream[snd->getName()] = snd;
			break;
		}
	}
}

void HDX9SoundManager::_onSoundDestroy(HDX9Sound *snd) {
	switch(snd->mtype) {
		case HDX9ST_FAF: {
			mfmod_sounds_faf.erase(snd->getName());
			break;
		}
		case HDX9ST_STREAM: {
			mfmod_sounds_stream.erase(snd->getName());
			break;
		}
	}
}

void HDX9SoundManager::channelEffectFadeVolume(FMOD::Channel *channel,float time,float startvolume,float endvolume) {
	struct ChannelEffectFadeVolume : public HDX9SoundChannelEffectBase {
		float mtimemax;
		float mtimeleft;

		float mvolumestart;
		float mvolumeend;

		ChannelEffectFadeVolume() {
			setFunctionOnUpdate([this](float dt) {
				mtimeleft -= dt;
				mfmod_channel->setVolume(mvolumestart + (1.0f-((std::max)(mtimeleft,0.0f)/mtimemax))*(mvolumeend-mvolumestart));
			});
		}

		virtual bool isComplete() {
			return mtimeleft <= 0.0f;
		}
	};

	ChannelEffectFadeVolume *effect = new ChannelEffectFadeVolume();
	effect->mfmod_channel = channel;
	effect->mtimemax = effect->mtimeleft = time;
	effect->mvolumestart = startvolume;
	effect->mvolumeend = endvolume;
}