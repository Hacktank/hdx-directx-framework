#pragma once

class IHDXMenuEventHandler {
public:
	virtual void onMenuTransitionEnter(bool dotransition) = 0;
	virtual void onMenuTransitionExit(bool dotransition) = 0;

	virtual void update(float dt) = 0;
	virtual void render() = 0;
};
