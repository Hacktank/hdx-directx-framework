#pragma once

#include "hdx9ui_elementeffect_base.h"

class HDXUIElementEffect_TextDraw : public HDXUIElementEffect_Base {
public:
	HDXUIElementEffect_TextDraw(const std::string &name,
								HDXUIElementBase *parent,
								const std::string &font,
								const std::string &text,
								DWORD fontdrawflags,
								D3DXCOLOR color);
	virtual ~HDXUIElementEffect_TextDraw();

	std::string getFont() const;
	void setFont(const std::string &font);

	std::string getText() const;
	void setText(const std::string &text);

	DWORD getFontDrawFlags() const;
	void setFontDrawFlags(const DWORD &flags);

	D3DXCOLOR getFontDrawColor() const;
	void setFontDrawColor(const D3DXCOLOR &color);

	void getFontDrawRect(RECT *rect) const;
	void setFontDrawRect(const RECT *rect);

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransiton);

	virtual void update(float dt);
	virtual void render();

private:
	std::string mfont;
	std::string mtext;
	DWORD mfontdrawflags;
	RECT mfontdrawrect;
	D3DXCOLOR mfontdrawcolor;
};
