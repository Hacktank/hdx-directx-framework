#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>

#include <string>
#include <unordered_map>

#define HDX_FONTMAN HDXFontManager::instance()

RECT HDX_RECT_POINT(int x,int y);

class HDXFontBase {
	friend class HDXFontManager;
public:
	HDXFontBase(const std::string &name);
	virtual ~HDXFontBase();

	std::string getName();

	virtual void drawText(const char *str,const RECT &dest,DWORD format,D3DCOLOR color,ID3DXSprite *sprite = 0) = 0;
	virtual void calculateTextRect(const char *str,RECT *rect,DWORD format) = 0;

	//************************************
	// Method:    calculateFormattedTextDrawRect
	// Returns:   void
	// Parameter: const char * str
	// Parameter: RECT * rect
	// Parameter: DWORD format
	// Parameter: int x
	// Parameter: int y
	// Parameter: int w
	// Parameter: int h
	// Notes:
	// Calculates the draw rect for the specified parameters
	//************************************
	virtual void calculateFormattedTextDrawRect(const char *str,RECT *rect,DWORD format,int x,int y,int w,int h);

private:
	std::string mname;

	virtual void _onLostDevice() = 0;
	virtual void _onResetDevice() = 0;
};

class HDXFontDX2D : public HDXFontBase {
	friend class HDXFontManager;
public:
	HDXFontDX2D(const std::string &name);
	virtual ~HDXFontDX2D();

	virtual void drawText(const char *str,const RECT &dest,DWORD format,D3DCOLOR color,ID3DXSprite *sprite = 0);
	virtual void calculateTextRect(const char *str,RECT *rect,DWORD format);

private:
	ID3DXFont *md3dfont;

	bool _build(const char *facename,int pointsize,int weight,bool italic,DWORD quality);

	virtual void _onLostDevice();
	virtual void _onResetDevice();
};

class HDXFontDX3D : public HDXFontBase {
	friend class HDXFontManager;
public:
	HDXFontDX3D(const std::string &name);
	virtual ~HDXFontDX3D();

	virtual void drawText(const char *str,const RECT &dest,DWORD format,D3DCOLOR color,ID3DXSprite *sprite = 0);
	virtual void calculateTextRect(const char *str,RECT *rect,DWORD format);

private:
	ID3DXMesh *mmesh;

	bool _build(const char *facename,int pointsize,int weight,bool italic,DWORD quality);

	virtual void _onLostDevice();
	virtual void _onResetDevice();
};

class HDXFontManager {
	friend class HDX9;
public:
	static HDXFontManager* instance() { static HDXFontManager *gnew = new HDXFontManager(); return gnew; }
	~HDXFontManager();

	HDXFontBase* getFont(const std::string &name);
	void deleteFont(const std::string &name);

	HDXFontDX2D* createFontDX(const std::string &name,const char *facename,int pointsize,int weight = FW_NORMAL,bool italic = false,DWORD quality = DEFAULT_QUALITY);

private:
	std::unordered_map<std::string,HDXFontBase*> mfonts;

	HDXFontManager();

	void _onLostDevice();
	void _onResetDevice();
};
