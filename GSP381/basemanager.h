#pragma once

#include <unordered_map>
#include <vector>
#include <functional>
#include <cassert>

template<class DATA,class KEY>
class __BaseManager {
public:
	__BaseManager() {
	}

	virtual ~__BaseManager() {
	}

	void addItem(const KEY &id,const DATA &data) {
		assert(mitemmap.count(id)==0); //items must have unique key
		mitemmap[id] = data;
		mitemvector.push_back(&mitemmap[id]); //possible issue here, if problems arise use new above
	}

	bool hasItem(const KEY &id) const {
		return mitemmap.count(id)>0;
	}

	DATA& getItem(const KEY &id) {
		//assert(hasItem(id));
		return mitemmap[id];
	}

	const DATA& getItem(const KEY &id) const {
		assert(hasItem(id));
		return mitemmap.find(id)->second;
	}

	void setItem(const KEY &id,const DATA &data) {
		assert(hasItem(id));
		mitemmap.find(id)->second = data;
	}

	void removeItem(const KEY &id) {
		auto &fitmap = mitemmap.find(id);
		if(fitmap != mitemmap.end()) {
			mitemvector.erase(std::find(mitemvector.begin(),mitemvector.end(),&fitmap->second));
			mitemmap.erase(fitmap);
		}
	}

	unsigned size() const {
		return mitemvector.size();
	}

	void clear() {
		mitemmap.clear();
		mitemvector.clear();
	}

	void doIterate(std::function<bool(DATA &item)> &&func) {
		for(unsigned int i = 0; i < mitemvector.size(); i++) {
			if(func(*mitemvector[i])) break;
		}
	}

	void doIterate(std::function<bool(const DATA &item)> &&func) const {
		for(unsigned int i = 0; i < mitemvector.size(); i++) {
			if(func(*mitemvector[i])) break;
		}
	}

private:
	std::unordered_map<KEY,DATA> mitemmap;
	std::vector<DATA*> mitemvector;
};

template<class DATA,class KEY = int>
class BaseManagerKey : public __BaseManager<DATA,KEY> {
public:
	BaseManagerKey() {
	}

	virtual ~BaseManagerKey() {
	}
};

template<class DATA>
class BaseManagerID : public __BaseManager<DATA,int> {
private:
	__BaseManager<DATA,int>::addItem; //custom version
public:
	BaseManagerID() {
		mnvid = 0;
	}

	virtual ~BaseManagerID() {
	}

	int addItem(const DATA &data) {
		int newid = _getNVID();
		addItem(newid,data);
		return newid;
	}

	//public version, does not increment counter
	int getNVID() {
		return mnvid;
	}

private:
	int mnvid;

	int _getNVID() {
		return mnvid++;
	}
};
