
#pragma once

#include "hdx9camera.h"

#include <vector>
#include <functional>

class HDXCamera_Mouselook : public HDXCamera {
	struct Control {
		int mkey;
		std::vector<int> mkeys;
		bool mpressed;
	};

	enum {
		CML_F = 0,
		CML_L,
		CML_B,
		CML_R,
		CML_U,
		CML_D,
		CML_SPD_FINE,
		CML_SPD_FAST,
		CML_SHIFT,

		CML_NUM
	};

public:
	HDXCamera_Mouselook();
	virtual ~HDXCamera_Mouselook();

	void initialize(float sensitivity_x,
					float sensitivity_y,
					float movespeed,
					int key_f,
					int key_l,
					int key_b,
					int key_r,
					int key_u,
					int key_d,
					int key_spd_fine,
					int key_spd_fast,
					int key_shift);

	int getKeyF() const;
	void setKeyF(int key);

	int getKeyL() const;
	void setKeyL(int key);

	int getKeyB() const;
	void setKeyB(int key);

	int getKeyR() const;
	void setKeyR(int key);

	int getKeyU() const;
	void setKeyU(int key);

	int getKeyD() const;
	void setKeyD(int key);

	int getKeySpeedFine() const;
	void setKeySpeedFine(int key);

	int getKeySpeedFast() const;
	void setKeySpeedFast(int key);

	int getKeyShift() const;
	void setKeyShift(int key);

	float getSensitivityX() const;
	void setSensitivityX(float sen);

	float getSensitivityY() const;
	void setSensitivityY(float sen);

	float getMoveSpeed() const;
	void setMoveSpeed(float spd);

private:
	int mcbid_onupdate;
	int mcbid_onmousemove;
	int mcbid_onkeypress;
	int mcbid_onkeyrelease;
	int mcbid_onmousepress;
	int mcbid_onmouserelease;

	Control mcontrols[CML_NUM];
	float msensitivity_x,msensitivity_y;
	float mspeed_move;
};
