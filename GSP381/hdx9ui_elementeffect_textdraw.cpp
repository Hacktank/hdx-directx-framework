#include "hdx9ui_elementeffect_textdraw.h"

#include "hdx9.h"
#include "hdx9font.h"
#include "hdx9ui.h"

#include "hdx9color.h"

HDXUIElementEffect_TextDraw::HDXUIElementEffect_TextDraw(const std::string &name,
														 HDXUIElementBase *parent,
														 const std::string &font,
														 const std::string &text,
														 DWORD fontdrawflags,
														 D3DXCOLOR color) : HDXUIElementEffect_Base(name,parent) {
	mfont = font;
	mfontdrawflags = fontdrawflags;
	mfontdrawcolor = color;
	setText(text);

	SetRect(&mfontdrawrect,0,0,(int)parent->getActionWidth(),(int)parent->getActionHeight());
}

HDXUIElementEffect_TextDraw::~HDXUIElementEffect_TextDraw() {
}

std::string HDXUIElementEffect_TextDraw::getFont() const {
	return mfont;
}

void HDXUIElementEffect_TextDraw::setFont(const std::string &font) {
	mfont = font;
}

std::string HDXUIElementEffect_TextDraw::getText() const {
	return mtext;
}

void HDXUIElementEffect_TextDraw::setText(const std::string &text) {
	mtext = text;
}

DWORD HDXUIElementEffect_TextDraw::getFontDrawFlags() const {
	return mfontdrawflags;
}

void HDXUIElementEffect_TextDraw::setFontDrawFlags(const DWORD &flags) {
	mfontdrawflags = flags;
}

D3DXCOLOR HDXUIElementEffect_TextDraw::getFontDrawColor() const {
	return mfontdrawcolor;
}

void HDXUIElementEffect_TextDraw::setFontDrawColor(const D3DXCOLOR &color) {
	mfontdrawcolor = color;
}

void HDXUIElementEffect_TextDraw::getFontDrawRect(RECT *rect) const {
	*rect = mfontdrawrect;
}

void HDXUIElementEffect_TextDraw::setFontDrawRect(const RECT *rect) {
	mfontdrawrect = *rect;
}

void HDXUIElementEffect_TextDraw::onMenuTransitionEnter(bool dotransition) {
}

void HDXUIElementEffect_TextDraw::onMenuTransitionExit(bool dotransiton) {
}

void HDXUIElementEffect_TextDraw::update(float dt) {
}

void HDXUIElementEffect_TextDraw::render() {
	HDXFontBase *testfont = HDX_FONTMAN->getFont(mfont);
	if(testfont) {
		ID3DXSprite *com_sprite = HDX_MAIN->getD3DSprite();

		D3DXMATRIX trans_parent;
		getParent()->getTransform(&trans_parent);

		com_sprite->SetTransform(&trans_parent);

		D3DXCOLOR pcol;
		if(getParent()) {
			pcol = getParent()->getBlendColor();
		} else {
			pcol = 0xffffffff;
		}
		D3DXCOLOR mcol = getFontDrawColor();
		D3DXCOLOR tcol = colorMultiply(pcol,mcol);

		testfont->drawText(mtext.c_str(),mfontdrawrect,mfontdrawflags,tcol,com_sprite);
	}
}