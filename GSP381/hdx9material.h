
#pragma once

#include <d3d9.h>
#include <d3dx9.h>

#include "hdx9color.h"

struct HDXMaterial {
	D3DXCOLOR diffuse;
	D3DXCOLOR ambient;
	D3DXCOLOR specular;
	D3DXCOLOR emisive;
	float power;

	HDXMaterial() {

	};

	HDXMaterial(D3DXCOLOR pdiffuse,
				D3DXCOLOR pambient,
				D3DXCOLOR pspecular,
				D3DXCOLOR pemissive,
				float ppower) {
		diffuse = pdiffuse;
		ambient = pambient;
		specular = pspecular;
		emisive = pemissive;
		power = ppower;
	}
};

#define HDXMAT_BLACK (HDXMaterial(HDXCOL_BLACK, HDXCOL_BLACK, HDXCOL_BLACK, HDXCOL_BLACK, 0.0f))
#define HDXMAT_WHITE (HDXMaterial(HDXCOL_WHITE, HDXCOL_WHITE, HDXCOL_WHITE, HDXCOL_BLACK, 10.0f))
#define HDXMAT_RED   (HDXMaterial(HDXCOL_RED  , HDXCOL_RED  , HDXCOL_WHITE, HDXCOL_BLACK, 10.0f))
#define HDXMAT_GREEN (HDXMaterial(HDXCOL_GREEN, HDXCOL_GREEN, HDXCOL_WHITE, HDXCOL_BLACK, 10.0f))
#define HDXMAT_BLUE  (HDXMaterial(HDXCOL_BLUE , HDXCOL_BLUE , HDXCOL_WHITE, HDXCOL_BLACK, 10.0f))
