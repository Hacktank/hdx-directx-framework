
#include "hdx9color.h"

#include <d3dx9.h>

D3DXCOLOR colorAverage(const D3DXCOLOR &a,const D3DXCOLOR &b) {
	D3DXCOLOR ret;
	for(int i = 0; i < 4; i++) {
		((float*)ret)[i] = (((const float*)a)[i] + ((const float*)b)[i]) / 2.0f;
	}
	return ret;
}

D3DXCOLOR colorMultiply(const D3DXCOLOR &a,const D3DXCOLOR &b) {
	D3DXCOLOR ret;
	for(int i = 0; i < 4; i++) {
		((float*)ret)[i] = (((const float*)a)[i] * ((const float*)b)[i]);
	}
	return ret;
}
