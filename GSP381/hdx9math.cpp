
#include "hdx9math.h"

D3DXVECTOR3 matrixGetRow(const D3DXMATRIX &mat,int ind) {
	return D3DXVECTOR3(mat[HDX9MATH_IJ(0,ind)],mat[HDX9MATH_IJ(1,ind)],mat[HDX9MATH_IJ(2,ind)]);
}

D3DXVECTOR3 matrixGetColumn(const D3DXMATRIX &mat,int ind) {
	return D3DXVECTOR3(mat[HDX9MATH_IJ(ind,0)],mat[HDX9MATH_IJ(ind,1)],mat[HDX9MATH_IJ(ind,2)]);
}

void matrixSetRow(D3DXMATRIX *mat,int ind,const D3DXVECTOR3 &vec) {
	for(int i = 0; i < 3; i++) {
		(*mat)[HDX9MATH_IJ(i,ind)] = vec[i];
	}
}

void matrixSetColumn(D3DXMATRIX *mat,int ind,const D3DXVECTOR3 &vec) {
	for(int i = 0; i < 3; i++) {
		(*mat)[HDX9MATH_IJ(ind,i)] = vec[i];
	}
}

D3DXVECTOR3 matrixGetScale(const D3DXMATRIX &mat) {
	const D3DXVECTOR3 vx = matrixGetRow(mat,0);
	const D3DXVECTOR3 vy = matrixGetRow(mat,1);
	const D3DXVECTOR3 vz = matrixGetRow(mat,2);
	return D3DXVECTOR3(D3DXVec3Length(&vx),
					   D3DXVec3Length(&vy),
					   D3DXVec3Length(&vz));
}

D3DXMATRIX matrixLookDirection(const D3DXVECTOR3 &eye,const D3DXVECTOR3 &dir) {
 	D3DXVECTOR3 axis_x;
 	D3DXVECTOR3 axis_y;
 	D3DXVECTOR3 axis_z;
 
 	D3DXVec3Normalize(&axis_z,&dir);
 
 	D3DXVec3Cross(&axis_x,&YUNIT3,&axis_z);
 	D3DXVec3Normalize(&axis_x,&axis_x);
 	D3DXVec3Cross(&axis_y,&axis_z,&axis_x);
 
 	D3DXMATRIX ret;
 
 	D3DXMatrixIdentity(&ret);
 
 	matrixSetColumn(&ret,0,axis_x);
	matrixSetColumn(&ret,1,axis_y);
	matrixSetColumn(&ret,2,axis_z);
 
 	ret[HDX9MATH_IJ(0,3)] = -D3DXVec3Dot(&axis_x,&eye);
 	ret[HDX9MATH_IJ(1,3)] = -D3DXVec3Dot(&axis_y,&eye);
 	ret[HDX9MATH_IJ(2,3)] = -D3DXVec3Dot(&axis_z,&eye);

	return ret;
}

D3DXVECTOR3 vector3TransformQuaternion(const D3DXVECTOR3 &v,const D3DXQUATERNION &q) {
	D3DXQUATERNION v_quat(v.x,v.y,v.z,0.0f);
	D3DXQUATERNION q_inv;
	D3DXQuaternionInverse(&q_inv,&q);
	D3DXQUATERNION ret_quat = q_inv * v_quat * q;
	return D3DXVECTOR3(ret_quat.x,ret_quat.y,ret_quat.z);
}

D3DXQUATERNION quaterionRotateTo(const D3DXVECTOR3 &q,const D3DXVECTOR3 &p) {
	D3DXQUATERNION ret;

	float dot = D3DXVec3Dot(&q,&p);
	if(dot <= -0.9995f) {
		D3DXQuaternionRotationAxis(&ret,&YUNIT3,-(float)M_PI); //left handed, remove negation for right handed
	} else if(dot >= 0.9995f) {
		ret = D3DXQUATERNION(0,0,0,1);
	} else {
		D3DXVECTOR3 tmp;
		D3DXVec3Cross(&tmp,&q,&p); //left handed, swap q and p for right handed
		D3DXQuaternionNormalize(&ret,&D3DXQUATERNION(tmp.x,tmp.y,tmp.z,1+dot));
	}

	return ret;
}
