#include "hdx9ui_elementeffect_base.h"

#include "hdx9ui.h"

HDXUIElementEffect_Base::HDXUIElementEffect_Base(const std::string &name,HDXUIElementBase *parent) {
	mname = name;
	mparent = parent;
	mactive = true;
	menabled = true;
}

HDXUIElementEffect_Base::~HDXUIElementEffect_Base() {
}

std::string HDXUIElementEffect_Base::getName() const {
	return mname;
}

HDXUIElementBase* HDXUIElementEffect_Base::getParent() const {
	return mparent;
}

bool HDXUIElementEffect_Base::isActive() const {
	return mactive;
}

void HDXUIElementEffect_Base::setActive(bool active) {
	mactive = active;
}

bool HDXUIElementEffect_Base::isEnabled() const {
	return menabled;
}

void HDXUIElementEffect_Base::setEnabled(bool enabled) {
	menabled = mactive;
}

float HDXUIElementEffect_Base::getZPos() const {
	return mparent->getZPos();
}

float HDXUIElementEffect_Base::getDesiredTransitionTime() const {
	return 0.0f;
}

void HDXUIElementEffect_Base::onMenuTransitionEnter(bool dotransition) {
}

void HDXUIElementEffect_Base::onMenuTransitionExit(bool dotransiton) {
}

void HDXUIElementEffect_Base::update(float dt) {
}

void HDXUIElementEffect_Base::render() {
}