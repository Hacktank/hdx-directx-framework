#pragma once

#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <directxmath.h>

//#define DIRECTINPUT_VERSION 0x0800
//#include <dinput.h>

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"dxerr.lib")
#pragma comment(lib,"dinput8.lib")
//#pragma comment(lib,"dxguid.lib")

#ifdef _DEBUG
#pragma comment(lib, "d3dx9d.lib")
#else
#pragma comment(lib, "d3dx9.lib")
#endif

#include <chrono>
#include <string>
#include <functional>

#include "basemanager.h"

#define DXERR_FATAL(errormsg) do {MessageBox(0,errormsg"\n\nApplication will now exit","Error",0); PostQuitMessage(0);} while(0)
#define DXERR_FATAL_IF(pred,errormsg) do {if(pred) DXERR_FATAL(errormsg);} while(0)

#define WNDCLASSNAME "_HTDX9"

#define HDX_MAIN HDX9::instance()

#include "hdx9math.h"

#define FRAND() (rand()/(float)RAND_MAX)
#define RANDRANGE(a,b) ((rand()%(int)((b)+1-(a)))+(int)(a))

#include "hdx9camera.h"

class HDX9App;

class HDX9 {
	friend int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int);

public:
	static HDX9* instance() { static HDX9 *gnew = new HDX9(); return gnew; }
	~HDX9();

	std::string getWindowTitle();
	void setWindowTitle(const std::string &title);

	bool getWindowBorderless();
	void setWindowBorderless(bool borderless);

	void getWindowPosition(int *x,int *y);
	void setWindowPosition(int x,int y);

	void getWindowSize(int *w,int *h);
	void setWindowSize(int w,int h);

	HWND getWindowHWND();
	HINSTANCE getHINSTANCE();

	DWORD getWindowCurrentStyle();

	D3DCOLOR getWindowClearColor();
	void setWindowClearColor(D3DCOLOR color);

	bool getWindowFullscreen();
	void setWindowFullscreen(bool fullscreen);

	bool getWindowVsync();
	void setWindowVsync(bool vsync);

	IDirect3DDevice9* getD3DDevice();
	ID3DXSprite* getD3DSprite();
	D3DPRESENT_PARAMETERS* getD3DPresentationParameters();

	HDXCamera* getCameraCurrent();
	void setCamera(HDXCamera *camera);
	void setCameraDefault();

	unsigned int getFPS();

	void step();

	int registerFuncOnDeviceLost(std::function<void(void)> &&func);
	void unregisterFuncOnDeviceLost(int id);
	int registerFuncOnDeviceReset(std::function<void(void)> &&func);
	void unregisterFuncOnDeviceReset(int id);

	int registerFuncUpdate(std::function<void(float dt)> &&func);
	void unregisterFuncUpdate(int id);
	int registerFuncRenderDevice(std::function<void(IDirect3DDevice9 *com)> &&func);
	void unregisterFuncRenderDevice(int id);
	int registerFuncRenderSprite(std::function<void(ID3DXSprite *com)> &&func);
	void unregisterFuncRenderSprite(int id);

private:
	HINSTANCE mhinstance;

	HDX9App *mapp;

	IDirect3D9 *mcom_d3d;
	IDirect3DDevice9 *mcom_d3ddevice;
	ID3DXSprite *mcom_d3dsprite;
	D3DPRESENT_PARAMETERS md3dpp;
	D3DCAPS9 md3dcaps;
	DWORD mdevicebehaviorflags;
	HWND mwnd_hwnd;
	bool mwnd_vsync;
	bool mwnd_fullscreen;
	bool mwnd_borderless;
	int mwnd_windowedwidth;
	int mwnd_windowedheight;
	int mwnd_windowedx;
	int mwnd_windowedy;
	D3DCOLOR mwnd_clearcolor;
	std::string mwnd_title;
	unsigned mframes_cur;
	unsigned mframes_prev;

	HDXCamera mcamera_default;
	HDXCamera *mcamera_cur;

	std::chrono::high_resolution_clock::time_point mframes_prevtime,mupdate_prevtime;

	BaseManagerID<std::function<void(void)>> mfunc_ondevice_lost,mfunc_ondevice_reset;
	BaseManagerID<std::function<void(float dt)>> mfunc_update;
	BaseManagerID<std::function<void(IDirect3DDevice9 *com)>> mfunc_render_device;
	BaseManagerID<std::function<void(ID3DXSprite *com)>> mfunc_render_sprite;

	HDX9();

	static LRESULT WINAPI _WndProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam);

	void _initialize(HDX9App *app,HINSTANCE hinstance,int x,int y,int w,int h);
	void _deinitialize();

	void _onLostDevice();
	HRESULT _resetDevice();
	void _onResetDevice();

	DWORD _getWindowStyle(bool fullscreen,bool border);
};