#pragma once

#include "messagemanager.h"

class MessageListener {
public:
	MessageListener() {
	}

	virtual ~MessageListener() {
		for(unsigned int i = 0; i < msubs.size(); i++) MSGMAN->removeSubscription(msubs[i]);
	}

	//************************************
	// Method:    addMessageSubscription
	// Returns:   int (the new subscription id)
	// Parameter: int sid (sender id to listen to)
	// Parameter: std::string type (message type to listen to)
	// Parameter: std::function<void(Args...)> &&func (callback)
	// Notes:
	// Use MSGCBFROMLAM(capture,args) {code} to generate the function object from a lambda
	// Use MSGCBFROMLFP(func,args) to generate the function object from a function pointer
	//************************************
	template<typename ...Args>
	int addMessageSubscription(int sid,std::string type,std::function<void(Args...)> &&func) {
		msubs.push_back(MSGMAN->addSubscription(std::forward<int>(sid),std::forward<std::string>(type),std::forward<std::function<void(Args...)>>(func)));
		return msubs.back();
	}

	//************************************
	// Method:    removeMessageSubscription
	// Returns:   void
	// Parameter: int id (subscription id, returned from addSubscription()
	// Notes:
	//************************************
	void removeMessageSubscription(int id) {
		auto fit = std::find(msubs.begin(),msubs.end(),id);
		if(fit != msubs.end()) msubs.erase(fit);
		MSGMAN->removeSubscription(id);
	}

private:
	std::vector<int> msubs;
};
