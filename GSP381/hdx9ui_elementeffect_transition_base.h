#pragma once

#include "hdx9ui_elementeffect_base.h"

#define HDX_TRANS_ENTER 0x00000001
#define HDX_TRANS_EXIT 0x00000002

class HDXUIElementEffect_Transition_Base : virtual public HDXUIElementEffect_Base {
public:
	HDXUIElementEffect_Transition_Base(const std::string &name,
									   HDXUIElementBase *parent,
									   DWORD runflags,
									   float transtime);
	virtual ~HDXUIElementEffect_Transition_Base();

	DWORD getRunFlags() const;
	void setRunFlags(DWORD runflags);

	float getTransTime() const;
	void setTransTime(float time);

	float getTransCounter() const;
	void setTransCounter(float counter);

	float getProgress() const;

	bool isTransitionDone() const;

	virtual float getDesiredTransitionTime() const;

	virtual void transition_onStart(bool enter) = 0;
	virtual void transition_onFinish() = 0;
	virtual void transition_onUpdate(float dt) = 0;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransition);

	virtual void update(float dt);

private:
	DWORD mrunflags;
	float mtranstime,mtranscounter;
};