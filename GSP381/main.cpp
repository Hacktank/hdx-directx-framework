#include <windows.h>
#pragma comment(lib, "Winmm.lib") //timeGetTime()

#include "hdx9.h"
#include "hdx9sound.h"
#include "hdx9app.h"

int WINAPI WinMain(HINSTANCE hinstance,HINSTANCE hprevinstance,LPSTR lpcmdline,int ncmdshow) {
	srand((unsigned int)timeGetTime());

	if(HDX_SOUNDMAN->initializeFMOD(500) != FMOD_OK) return 1;

	HDX9App app;

	HDX_MAIN->_initialize(&app,hinstance,100,100,640,480);

	app.initializeScene();

	//will be automatically called each frame
	HDX_MAIN->registerFuncUpdate([&](float dt) {
		app.updateScene(dt);
	});

	HDX_MAIN->registerFuncRenderDevice([&](IDirect3DDevice9 *com) {
		app.renderScene();
	});

	MSG msg;
	memset(&msg,0,sizeof(MSG));

	while(msg.message != WM_QUIT) {
		if(PeekMessage(&msg,0,0,0,PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			HDX_SOUNDMAN->update();
			HDX_MAIN->step();
		}
	}

	app.deinitializeScene();

	HDX_MAIN->_deinitialize();

	return 0;
}