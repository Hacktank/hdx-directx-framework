#include "hdx9ui_element_alignment.h"

HDXUIElement_Alignment::HDXUIElement_Alignment(const std::string name,HDXUIElementBase *parent,DWORD flags,float dirx,float diry,float posx,float posy,float padding) : HDXUIElementBase(name,parent) {
	setFlags(flags);
	setPadding(padding);
	setOffset(0,0);
	setDirection(dirx,diry);
	setCurrentPostion(posx,posy);
}

HDXUIElement_Alignment::~HDXUIElement_Alignment() {
}

D3DXVECTOR2 HDXUIElement_Alignment::getDirection() const {
	return mdir;
}

void HDXUIElement_Alignment::getDirection(D3DXVECTOR2 *dir) const {
	*dir = mdir;
}

void HDXUIElement_Alignment::setDirection(const D3DXVECTOR2 *dir) {
	mdir = *dir;
}

void HDXUIElement_Alignment::setDirection(const float &x,const float &y) {
	mdir.x = x;
	mdir.y = y;
}

D3DXVECTOR2 HDXUIElement_Alignment::getCurrentPostion() const {
	return mcurpos;
}

void HDXUIElement_Alignment::getCurrentPostion(D3DXVECTOR2 *pos) const {
	*pos = mcurpos;
}

void HDXUIElement_Alignment::setCurrentPostion(const D3DXVECTOR2 *pos) {
	mcurpos = *pos;
}

void HDXUIElement_Alignment::setCurrentPostion(const float &x,const float &y) {
	mcurpos.x = x;
	mcurpos.y = y;
}

D3DXVECTOR2 HDXUIElement_Alignment::getNextPostion(HDXUIElementBase *newelement) {
	D3DXVECTOR2 ret;
	getNextPostion(&ret,newelement);
	return ret;
}

void HDXUIElement_Alignment::getNextPostion(D3DXVECTOR2 *pos,HDXUIElementBase *newelement) {
	D3DXVECTOR2 newext(newelement->getActionWidth(),newelement->getActionHeight());
	float len = D3DXVec2Dot(&mdir,&newext);
	D3DXVECTOR2 alignment(0,0);

	if(mflags&HDXAL_CENTER) {
		// nothing
	}

	if(mflags&HDXAL_LEFT) {
		alignment.x = newext.x/2;
	}

	if(mflags&HDXAL_RIGHT) {
		alignment.x = -newext.x/2;
	}

	if(mflags&HDXAL_VCENTER) {
		// nothing
	}

	if(mflags&HDXAL_TOP) {
		alignment.y = newext.y/2;
	}

	if(mflags&HDXAL_BOTTOM) {
		alignment.y = -newext.y/2;
	}

	*pos = getPosition() + mcurpos + moffset + alignment;
	mcurpos += mdir * (len+mpadding);
}

void HDXUIElement_Alignment::jumpDistance(float dst) {
	mcurpos += mdir * dst;
}

DWORD HDXUIElement_Alignment::getFlags() const {
	return mflags;
}

void HDXUIElement_Alignment::setFlags(const DWORD &flags) {
	mflags = flags;
}

D3DXVECTOR2 HDXUIElement_Alignment::getOffset() const {
	return moffset;
}

void HDXUIElement_Alignment::getOffset(D3DXVECTOR2 *pos) const {
	*pos = moffset;
}

void HDXUIElement_Alignment::setOffset(const D3DXVECTOR2 *pos) {
	moffset = *pos;
}

void HDXUIElement_Alignment::setOffset(const float &x,const float &y) {
	moffset.x = x;
	moffset.y = y;
}

float HDXUIElement_Alignment::getPadding() const {
	return mpadding;
}

void HDXUIElement_Alignment::setPadding(const float &padding) {
	mpadding = padding;
}

void HDXUIElement_Alignment::_addChild(HDXUIElementBase *child) {
	HDXUIElementBase::_addChild(child);
	child->setPosition(&getNextPostion(child));
}