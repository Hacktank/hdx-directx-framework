#include "hdx9ui_elementeffect_texturedraw.h"

#include "hdx9.h"
#include "hdx9texture.h"
#include "hdx9ui.h"

#include "hdx9color.h"

HDXUIElementEffect_TextureDraw::HDXUIElementEffect_TextureDraw(const std::string &name,
															   HDXUIElementBase *parent,
															   const std::string &tex) : HDXUIElementEffect_Base(name,parent) {
	mtexture = HDXTextureCreateFromFile(tex,true);
	mblendcolor = 0xffffffff; //white, no blend
	setSourceRectToWholeTexture();
	setSourceCenterToCenterSourceRect();
}

HDXUIElementEffect_TextureDraw::~HDXUIElementEffect_TextureDraw() {
	mtexture->release();
}

std::string HDXUIElementEffect_TextureDraw::getTexture() const {
	return mtexture->getName();
}

void HDXUIElementEffect_TextureDraw::setTexture(const std::string &tex) {
	mtexture->release();
	mtexture = HDXTextureCreateFromFile(tex,true);
}

D3DXCOLOR HDXUIElementEffect_TextureDraw::getBlendColor() const {
	return mblendcolor;
}

void HDXUIElementEffect_TextureDraw::setBlendColor(D3DXCOLOR blendcolor) {
	mblendcolor = blendcolor;
}

void HDXUIElementEffect_TextureDraw::getSourceRect(RECT *rect) const {
	*rect = msource_rect;
}

void HDXUIElementEffect_TextureDraw::setSourceRect(const RECT *rect) {
	msource_rect = *rect;
}

int HDXUIElementEffect_TextureDraw::getSourceWidth() const {
	return (msource_rect.right-msource_rect.left);
}

int HDXUIElementEffect_TextureDraw::getSourceHeight() const {
	return (msource_rect.bottom-msource_rect.top);
}

void HDXUIElementEffect_TextureDraw::setSourceRectToWholeTexture() {
	SetRect(&msource_rect,0,0,mtexture->getWidth(),mtexture->getHeight());
}

D3DXVECTOR3 HDXUIElementEffect_TextureDraw::getSourceCenter() const {
	D3DXVECTOR3 ret;
	getSourceCenter(&ret);
	return ret;
}

void HDXUIElementEffect_TextureDraw::getSourceCenter(D3DXVECTOR3 *center) const {
	*center = msource_center;
}

void HDXUIElementEffect_TextureDraw::setSourceCenter(const D3DXVECTOR3 *center) {
	msource_center = *center;
}

void HDXUIElementEffect_TextureDraw::setSourceCenterToCenterSourceRect() {
	msource_center.x = (msource_rect.right-msource_rect.left)/2.0f;
	msource_center.y = (msource_rect.bottom-msource_rect.top)/2.0f;
	msource_center.z = 0;
}

float HDXUIElementEffect_TextureDraw::getZPos() const {
	return HDXUI_GETZABOVE(getParent()->getZPos());
}

void HDXUIElementEffect_TextureDraw::onMenuTransitionEnter(bool dotransition) {
}

void HDXUIElementEffect_TextureDraw::onMenuTransitionExit(bool dotransiton) {
}

void HDXUIElementEffect_TextureDraw::update(float dt) {
}

void HDXUIElementEffect_TextureDraw::render() {
	ID3DXSprite *com_sprite = HDX_MAIN->getD3DSprite();

	D3DXMATRIX trans_parent;
	getParent()->getTransform(&trans_parent);

	D3DXVECTOR3 pos;
	pos.x = pos.y = 0;
	pos.z = getZPos();

	com_sprite->SetTransform(&trans_parent);

	D3DXCOLOR pcol;
	if(getParent()) {
		pcol = getParent()->getBlendColor();
	} else {
		pcol = 0xffffffff;
	}
	D3DXCOLOR mcol = getBlendColor();
	D3DXCOLOR tcol = colorMultiply(pcol,mcol);

	com_sprite->Draw(mtexture->getD3DTexture(),&msource_rect,&msource_center,&pos,tcol);

	com_sprite->Flush();
}