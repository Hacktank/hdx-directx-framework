#include "hdx9ui.h"

#include "hdx9.h"
#include "hdx9inputmanager.h"
#include "hdx9intersection.h"

#include "hdx9ui_elementeffect_base.h"

#include "hdx9color.h"

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_utils.hpp>

void rectSetPosSize(RECT *rect,int x,int y,int w,int h) {
	rect->left = x-w/2;
	rect->top = y-h/2;
	rect->right = x+w/2;
	rect->bottom = y+h/2;
}

void rectShrink(RECT *rect,int sx,int sy) {
	rect->left += sx/2;
	rect->right -= sx/2;
	rect->top += sy/2;
	rect->bottom -= sy/2;
}

void rectGrow(RECT *rect,int gx,int gy) {
	rectShrink(rect,-gx,-gy);
}

void rectGetSubRect(RECT *rect,int sw,int sh,int i,int j) {
	int x = sw*i;
	int y = sh*j;

	SetRect(rect,x,y,x+sw,y+sh);
}

HDXUIElementBase::HDXUIElementBase(const std::string name,HDXUIElementBase *parent) {
	mname = name;
	mparent = parent;

	mactionwidth = mactionheight = 0;
	mpos.x = mpos.y = 0;
	mscale.x = mscale.y = 1.0f;
	mrotcenter.x = mrotcenter.y = 0;
	mrot = 0;
	mzpos = 0.5f;

	mactive = true;
	menabled = true;
	mmousepressed = false;
	mwasmouseover = false;

	mblendcolor = 0xffffffff;
}

HDXUIElementBase::~HDXUIElementBase() {
	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		delete item;
		return false;
	});
	meffects.clear();

	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		delete item;
		return false;
	});
	mchilderen.clear();

	for(auto it = mcallbackid_onmousepress.begin(); it != mcallbackid_onmousepress.end(); it++) {
		HDX_INPUT->unregisterCallbackOnMousePress(*it);
	}
	mcallbackid_onmousepress.clear();

	for(auto it = mcallbackid_onkeypress.begin(); it != mcallbackid_onkeypress.end(); it++) {
		HDX_INPUT->unregisterCallbackOnKeyPress(*it);
	}
	mcallbackid_onkeypress.clear();
}

std::string HDXUIElementBase::getName() const {
	return mname;
}

HDXUIElementBase* HDXUIElementBase::getParent() const {
	return mparent;
}

bool HDXUIElementBase::isActive() const {
	return mactive;
}

void HDXUIElementBase::setActive(bool active) {
	if(mactive && !active) {
		mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
			if(item->isActive()) {
				mreactivate_childeren.push_back(item);
				item->setActive(false);
			}
			return false;
		});
		meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
			if(item->isActive()) {
				mreactivate_effects.push_back(item);
				item->setActive(false);
			}
			return false;
		});
	}
	if(!mactive && active) {
		for(auto it = mreactivate_childeren.begin(); it != mreactivate_childeren.end(); it++) {
			(*it)->setActive(true);
		}
		for(auto it = mreactivate_effects.begin(); it != mreactivate_effects.end(); it++) {
			(*it)->setActive(true);
		}
	}
	mactive = active;
}

bool HDXUIElementBase::isEnabled() const {
	return menabled;
}

void HDXUIElementBase::setEnabled(bool enabled) {
	if(menabled && !enabled) {
		mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
			if(item->isEnabled()) {
				mreenable_childeren.push_back(item);
				item->setEnabled(false);
			}
			return false;
		});
		meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
			if(item->isEnabled()) {
				mreenable_effects.push_back(item);
				item->setEnabled(false);
			}
			return false;
		});
	}
	if(!menabled && enabled) {
		for(auto it = mreactivate_childeren.begin(); it != mreactivate_childeren.end(); it++) {
			(*it)->setEnabled(true);
		}
		for(auto it = mreactivate_effects.begin(); it != mreactivate_effects.end(); it++) {
			(*it)->setEnabled(true);
		}
	}
	menabled = enabled;
}

void HDXUIElementBase::getTransform(D3DXMATRIX *mat) const {
	//recursively get this element's transform based on the whole menu hierarchy
	D3DXMATRIX parenttrans;
	if(mparent) {
		mparent->getTransform(&parenttrans);
	} else {
		D3DXMatrixIdentity(&parenttrans);
	}
	D3DXMATRIX mytrans;

	D3DXMatrixTransformation2D(&mytrans,0,0.0,&mscale,&mrotcenter,mrot,&mpos);
	*mat = mytrans * parenttrans;
}

void HDXUIElementBase::getPosition(D3DXVECTOR2 *pos) const {
	*pos = mpos;
}

D3DXVECTOR2 HDXUIElementBase::getPosition() const {
	D3DXVECTOR2 ret;
	getPosition(&ret);
	return ret;
}

void HDXUIElementBase::setPosition(const D3DXVECTOR2 *pos) {
	mpos = *pos;
}

void HDXUIElementBase::setPosition(const float &x,const float &y) {
	mpos.x = x;
	mpos.y = y;
}

void HDXUIElementBase::setPosition(const D3DXVECTOR2 &pos) {
	setPosition(&pos);
}

void HDXUIElementBase::getScale(D3DXVECTOR2 *scale) const {
	*scale = mscale;
}

D3DXVECTOR2 HDXUIElementBase::getScale() const {
	D3DXVECTOR2 ret;
	getScale(&ret);
	return ret;
}

void HDXUIElementBase::setScale(const D3DXVECTOR2 *scale) {
	mscale = *scale;
}

void HDXUIElementBase::setScale(const float &x,const float &y) {
	mscale.x = x;
	mscale.y = y;
}

void HDXUIElementBase::setScale(const D3DXVECTOR2 &scale) {
	setScale(&scale);
}

void HDXUIElementBase::getRotationCenter(D3DXVECTOR2 *center) const {
	*center = mrotcenter;
}

D3DXVECTOR2 HDXUIElementBase::getRotationCenter() const {
	D3DXVECTOR2 ret;
	getRotationCenter(&ret);
	return ret;
}

void HDXUIElementBase::setRotationCenter(const D3DXVECTOR2 *center) {
	mrotcenter = *center;
}

void HDXUIElementBase::setRotationCenter(const float &x,const float &y) {
	mrotcenter.x = x;
	mrotcenter.y = y;
}

void HDXUIElementBase::setRotationCenter(const D3DXVECTOR2 &center) {
	setRotationCenter(&mrotcenter);
}

float HDXUIElementBase::getRotation() const {
	return mrot;
}

void HDXUIElementBase::setRotation(const float &rot) {
	mrot = rot;
}

float HDXUIElementBase::getActionWidth() const {
	return mactionwidth;
}

void HDXUIElementBase::setActionWidth(const float &actionwidth) {
	mactionwidth = actionwidth;
}

float HDXUIElementBase::getActionHeight() const {
	return mactionheight;
}

void HDXUIElementBase::setActionHeight(const float &actionheight) {
	mactionheight = actionheight;
}

D3DXVECTOR2 HDXUIElementBase::getActionSize() const {
	return D3DXVECTOR2(getActionWidth(),getActionHeight());
}

void HDXUIElementBase::setActionSize(const D3DXVECTOR2 &size) {
	setActionSize(VEC2TOARG(size));
}

void HDXUIElementBase::setActionSize(const float &w,const float &h) {
	setActionWidth(w);
	setActionHeight(h);
}

void HDXUIElementBase::getEffectiveActionRect(RECT *rect) const {
	D3DXMATRIX mytrans;
	getTransform(&mytrans);
	D3DXVECTOR2 points_pretrans[4] = { //CCW
		{
			-mactionwidth/2,
			-mactionheight/2
		},
		{
			-mactionwidth/2,
			mactionheight/2
		},
		{
			mactionwidth/2,
			mactionheight/2
		},
		{
			mactionwidth/2,
			-mactionheight/2
		}
	};

	SetRect(rect,INT_MAX,INT_MAX,INT_MIN,INT_MIN);
	for(int i = 0; i < 4; i++) {
		D3DXVECTOR4 transpoint;
		D3DXVec2Transform(&transpoint,&points_pretrans[i],&mytrans);
		rect->left = (int)(std::min)((float)rect->left,transpoint.x);
		rect->right = (int)(std::max)((float)rect->right,transpoint.x);
		rect->top = (int)(std::min)((float)rect->top,transpoint.y);
		rect->bottom = (int)(std::max)((float)rect->bottom,transpoint.y);
	}
}

float HDXUIElementBase::getZPos() const {
	return mzpos;
}

void HDXUIElementBase::setZPos(const float &zpos) {
	mzpos = zpos;
}

float HDXUIElementBase::getMaxZPos() const {
	float ret = 0.0f;
	meffects.doIterate([&](const HDXUIElementEffect_Base *item)->bool {
		ret = (std::max)(ret,item->getZPos());
		return false;
	});
	mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
		ret = (std::max)(ret,item->getMaxZPos());
		return false;
	});
	return ret;
}

float HDXUIElementBase::getMinZPos() const {
	float ret = 1.0f;
	meffects.doIterate([&](const HDXUIElementEffect_Base *item)->bool {
		ret = (std::min)(ret,item->getZPos());
		return false;
	});
	mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
		ret = (std::min)(ret,item->getMaxZPos());
		return false;
	});
	return ret;
}

D3DXCOLOR HDXUIElementBase::getBlendColor() const {
	//recursively get this element's transform based on the whole menu hierarchy
	D3DXCOLOR parentblendcolor;
	if(mparent) {
		parentblendcolor = mparent->getBlendColor();
	} else {
		parentblendcolor = 0xffffffff;
	}

	return colorMultiply(parentblendcolor,mblendcolor);
}

void HDXUIElementBase::setBlendColor(D3DXCOLOR color) {
	mblendcolor = color;
}

bool HDXUIElementBase::isMouseOver() const {
	if(mactive && menabled) {
		D3DXMATRIX mytrans;
		getTransform(&mytrans);
		D3DXVECTOR3 ext(mactionwidth/2,mactionheight/2,0);
		D3DXVECTOR3 mouse((float)HDX_INPUT->getMousePostion().x,(float)HDX_INPUT->getMousePostion().y,0);

		return HDX9IntersectionTests::obb_point(&ext,&mytrans,&mouse);
	}
	return false;
}

int HDXUIElementBase::registerCallbackOnMousePress(std::function<void(WPARAM key)> &&func) {
	mcallbackid_onmousepress.push_back(HDX_INPUT->registerCallbackOnMousePress([this,func](WPARAM key)->void {
		if(mactive && menabled) {
			if(isMouseOver()) {
				mmousepressed = true;
				func(key);
			}
		}
	}));
	return mcallbackid_onmousepress.back();
}

void HDXUIElementBase::unregisterCallbackOnMousePress(int id) {
	HDX_INPUT->unregisterCallbackOnMousePress(id);
	std::remove(mcallbackid_onmousepress.begin(),mcallbackid_onmousepress.end(),id);
}

int HDXUIElementBase::registerCallbackOnMouseRelease(std::function<void(WPARAM key,float timeheld)> &&func) {
	mcallbackid_onmousepress.push_back(HDX_INPUT->registerCallbackOnMouseRelease([this,func](WPARAM key,float timeheld)->void {
		if(mactive && menabled) {
			if(mmousepressed) {
				func(key,timeheld);
			}
		}
	}));
	return mcallbackid_onmouserelease.back();
}

void HDXUIElementBase::unregisterCallbackOnMouseRelease(int id) {
	HDX_INPUT->unregisterCallbackOnMouseRelease(id);
	std::remove(mcallbackid_onmouserelease.begin(),mcallbackid_onmouserelease.end(),id);
}

int HDXUIElementBase::registerCallbackOnMouseEnter(std::function<void(void)> &&func) {
	return mcallback_onmouseenter.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnMouseEnter(int id) {
	mcallback_onmouseenter.removeItem(id);
}

int HDXUIElementBase::registerCallbackOnMouseLeave(std::function<void(void)> &&func) {
	return mcallback_onmouseleave.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnMouseLeave(int id) {
	mcallback_onmouseleave.removeItem(id);
}

int HDXUIElementBase::registerCallbackOnKeyPress(std::function<void(WPARAM key)> &&func) {
	mcallbackid_onkeypress.push_back(HDX_INPUT->registerCallbackOnKeyPress([this,func](WPARAM key)->void {
		if(mactive && menabled) {
			func(key);
		}
	}));
	return mcallbackid_onkeypress.back();
}

int HDXUIElementBase::registerCallbackOnTransitionEnter(std::function<void(bool dotransition)> &&func) {
	return mcallback_ontransitionenter.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnTransitionEnter(int id) {
	mcallback_ontransitionenter.removeItem(id);
}

int HDXUIElementBase::registerCallbackOnTransitionExit(std::function<void(bool dotransition)> &&func) {
	return mcallback_ontransitionexit.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnTransitionExit(int id) {
	mcallback_ontransitionexit.removeItem(id);
}

void HDXUIElementBase::unregisterCallbackOnKeyPress(int id) {
	HDX_INPUT->unregisterCallbackOnKeyPress(id);
	std::remove(mcallbackid_onkeypress.begin(),mcallbackid_onkeypress.end(),id);
}

const BaseManagerKey<HDXUIElementBase*,std::string>& HDXUIElementBase::getChilderen() const {
	return mchilderen;
}

const BaseManagerKey<HDXUIElementEffect_Base*,std::string>& HDXUIElementBase::getEffects() const {
	return meffects;
}

HDXUIElementEffect_Base* HDXUIElementBase::getEffect(const std::string &name) const {
	return meffects.hasItem(name) ? meffects.getItem(name) : 0;
}

void HDXUIElementBase::destroyEffect(const std::string &name) {
	HDXUIElementEffect_Base *effect = getEffect(name);
	if(effect) {
		delete effect;
		meffects.removeItem(name);
	}
}

HDXUIElementEffect_Base* HDXUIElementBase::getSubEffect(const std::string &name) const {
	HDXUIElementEffect_Base *ret = getEffect(name);
	if(ret==0) {
		mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
			ret = item->getSubEffect(name);
			return ret==0;
		});
	}
	return ret;
}

HDXUIElementBase* HDXUIElementBase::createChildElementFromXML(const std::string &name,const std::string &file) {
	rapidxml::file<> xmlfile(file.c_str());
	rapidxml::xml_document<> doc;
	doc.parse<0>(xmlfile.data());

	rapidxml::xml_node<> *node_root = doc.first_node("uielement");

	for(rapidxml::xml_node<> *node_element = node_root->first_node(); node_element; node_element = node_element->next_sibling()) {
		if(strcmp(node_element->name(),"button")==0) {
			std::string xmlstring = node_element->first_node("texturexml")->value();
		} else if(strcmp(node_element->name(),"checkbox")==0) {
		}
	}

	return 0;
}

HDXUIElementBase* HDXUIElementBase::getChildElement(const std::string &name) const {
	return mchilderen.hasItem(name) ? mchilderen.getItem(name) : 0;
}

void HDXUIElementBase::destroyChildElement(const std::string &name) {
	HDXUIElementBase *child = getChildElement(name);
	if(child) {
		delete child;
		mchilderen.removeItem(name);
	}
}

HDXUIElementBase* HDXUIElementBase::getSubElement(const std::string &name) const {
	HDXUIElementBase *ret = getChildElement(name);
	if(ret==0) {
		mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
			ret = item->getSubElement(name);
			return ret==0;
		});
	}
	return ret;
}

float HDXUIElementBase::getDesiredTransitionTime() const {
	return 0.0f;
}

float HDXUIElementBase::getMaxDesiredTransitionTime() const {
	float ret = getDesiredTransitionTime();
	meffects.doIterate([&](const HDXUIElementEffect_Base *item)->bool {
		ret = (std::max)(ret,item->getDesiredTransitionTime());
		return false;
	});
	mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
		ret = (std::max)(ret,item->getMaxDesiredTransitionTime());
		return false;
	});
	return ret;
}

void HDXUIElementBase::onMenuTransitionEnter(bool dotransition) {
	mcallback_ontransitionenter.doIterate([&](std::function<void(bool dotransition)> &difunc)->bool {
		difunc(dotransition);
		return false;
	});

	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		item->onMenuTransitionEnter(dotransition);
		return false;
	});
	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		item->onMenuTransitionEnter(dotransition);
		return false;
	});
}

void HDXUIElementBase::onMenuTransitionExit(bool dotransition) {
	mcallback_ontransitionexit.doIterate([&](std::function<void(bool dotransition)> &difunc)->bool {
		difunc(dotransition);
		return false;
	});

	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		item->onMenuTransitionExit(dotransition);
		return false;
	});
	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		item->onMenuTransitionExit(dotransition);
		return false;
	});
}

void HDXUIElementBase::update(float dt) {
	mmousepressed = false;

	if(isMouseOver()) {
		if(!mwasmouseover) {
			mcallback_onmouseenter.doIterate([&](std::function<void(void)> &difunc)->bool {
				difunc();
				return false;
			});
		}
		mwasmouseover = true;
	} else {
		if(mwasmouseover) {
			mcallback_onmouseleave.doIterate([&](std::function<void(void)> &difunc)->bool {
				difunc();
				return false;
			});
		}
		mwasmouseover = false;
	}

	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		if(item->isActive()) item->update(dt);
		return false;
	});

	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		if(item->isActive()) item->update(dt);
		return false;
	});
}

void HDXUIElementBase::render() {
	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		if(item->isActive()) item->render();
		return false;
	});

	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		item->render();
		return false;
	});
}

void HDXUIElementBase::_addChild(HDXUIElementBase *child) {
	if(!mactive) {
		child->setActive(false);
		mreactivate_childeren.push_back(child);
	}
	if(!menabled) {
		child->setEnabled(false);
		mreenable_childeren.push_back(child);
	}
	mchilderen.addItem(child->getName(),child);
}

void HDXUIElementBase::_addEffect(HDXUIElementEffect_Base *effect) {
	if(!mactive) {
		effect->setActive(false);
		mreactivate_effects.push_back(effect);
	}
	if(!menabled) {
		effect->setEnabled(false);
		mreenable_effects.push_back(effect);
	}
	meffects.addItem(effect->getName(),effect);
}

HDXUIMenu::HDXUIMenu(const std::string name) : HDXUIElementBase(name,0) {
	setActive(false);
}

HDXUIMenu::~HDXUIMenu() {
}

HDXUIMenuMap::HDXUIMenuMap() {
}

HDXUIMenuMap::~HDXUIMenuMap() {
	for(auto it = mtraversal.begin(); it != mtraversal.end(); it++) {
		delete *it;
	}
	mtraversal.clear();
}

HDXUIMenu* HDXUIMenuMap::createMenu(const std::string &name) {
	assert(name.size()>0);

	HDXUIMenu *newmenu = new HDXUIMenu(name);
	mmenus.addItem(name,newmenu);
	return newmenu;
}

HDXUIMenu* HDXUIMenuMap::getMenu(const std::string &name) {
	return mmenus.getItem(name);
}

void HDXUIMenuMap::destroyMenu(const std::string &name) {
	HDXUIMenu *newmenu = getMenu(name);
	if(newmenu) {
		delete newmenu;
		mmenus.removeItem(name);
	}
}

HDXUIMenu* HDXUIMenuMap::getCurrentMenu() {
	if(mtraversal.size() > 0) {
		return mtraversal.back();
	}
	return 0;
}

void HDXUIMenuMap::navigateToMenu(const std::string &menu,bool dotransnition) {
	HDXUIMenu *targetmenu = getMenu(menu);
	//if the targetmenu exists and mtraversal is either empty or the targetmenu is not the current menu
	if(mtraversal.size()==0 || (mtraversal.back()!=targetmenu)) {
		if(mtraversal.size() > 0) {
			//current menu onMenuTransitionExit
			mtransitions.emplace(menu,dotransnition ? mtraversal.back()->getMaxDesiredTransitionTime() : 0,false);
		}
		//new menu onMenuTransitionEnter
		mtransitions.emplace(menu,targetmenu&&dotransnition ? targetmenu->getMaxDesiredTransitionTime() : 0,true);
	}
}

void HDXUIMenuMap::navigateBackwards(bool dotransnition) {
	if(mtraversal.size()>1) {
		navigateToMenu(mtraversal[mtraversal.size()-2]->getName(),dotransnition);
	} else if(mtraversal.size()==1) {
		navigateToMenu("",dotransnition);
	}
}

void HDXUIMenuMap::doIterate(std::function<bool(HDXUIMenu *item)> &&func) {
	mmenus.doIterate([&](HDXUIMenu *item)->bool {
		return func(item);
	});
}

void HDXUIMenuMap::update(float dt) {
	if(mtransitions.size() > 0) {
		_transition &curtrans = mtransitions.front();
		if(!curtrans.mstarted) {
			curtrans.mstarted = true;
			if(curtrans.menter) {
				_setMenu(curtrans.mtarget);
				if(mtraversal.size()>0) {
					mtraversal.back()->setEnabled(false); //interaction disabled, still visible and still updates
					mtraversal.back()->onMenuTransitionEnter(curtrans.mtimer>0.0f);
				}
			} else if(mtraversal.size() > 0) {
				mtraversal.back()->setEnabled(false); //interaction disabled, still visible and still updates
				mtraversal.back()->onMenuTransitionExit(curtrans.mtimer>0.0f);
			}
		}
		if(curtrans.mtimer > 0) {
			curtrans.mtimer -= dt;
		}
		if(curtrans.mtimer <= 0) {
			if(curtrans.mextraframes == 0) {
				_setMenu(curtrans.mtarget);
				mtransitions.pop();
			} else {
				curtrans.mextraframes--;
			}
		}
	}

	if(mtraversal.size()>0) {
		if(mtraversal.back()->isActive()) mtraversal.back()->update(dt);
	}
}

void HDXUIMenuMap::render() {
	if(mtraversal.size()>0) {
		mtraversal.back()->render();
	}
}

void HDXUIMenuMap::_setMenu(const std::string &menu) {
	HDXUIMenu *targetmenu = getMenu(menu);
	if(mtraversal.size()==0 || (mtraversal.back()!=targetmenu)) {
		if(mtraversal.size()>0) {
			mtraversal.back()->setActive(false);
			//if the target menu is already in the traversal remove it and whatever is after it
			auto fit = std::find(mtraversal.begin(),mtraversal.end(),targetmenu);
			while(fit != mtraversal.end()) {
				fit = mtraversal.erase(fit);
			}
		}
		if(targetmenu) {
			mtraversal.push_back(targetmenu);
		} else {
			mtraversal.clear();
		}
	}
	if(targetmenu) {
		targetmenu->setActive(true);
		targetmenu->setEnabled(true);
	}
}