#include "hdx9intersection.h"
#include "hdx9math.h"

bool HDX9IntersectionTests::box_point(const D3DXVECTOR3 *boxext,const D3DXVECTOR3 *boxpos,const D3DXVECTOR3 *point) {
	D3DXVECTOR3 pt = *point - *boxpos;
	for(int i = 0; i < 3; i++) {
		if(abs(pt[i]) >(*boxext)[i]) return false;
	}
	return true;
}

bool HDX9IntersectionTests::box_point(const RECT *box,const POINT *point) {
	if(point->x < box->left ||
	   point->x > box->right ||
	   point->y < box->top ||
	   point->y > box->bottom) {
		return false;
	}
	return true;
}

bool HDX9IntersectionTests::obb_point(const D3DXVECTOR3 *boxext,const D3DXMATRIX *trans,const D3DXVECTOR3 *point) {
	D3DXMATRIX trans_inv;
	D3DXMatrixInverse(&trans_inv,0,trans);

	D3DXVECTOR4 relcenter;
	D3DXVec3Transform(&relcenter,point,&trans_inv);

	for(int i = 0; i < 3; i++) {
		if(abs(relcenter[i]) >(*boxext)[i]) return false;
	}

	return true;
}

bool HDX9IntersectionTests::obb_obb(const D3DXVECTOR3 *boxext0,const D3DXMATRIX *trans0,const D3DXVECTOR3 *boxext1,const D3DXMATRIX *trans1) {
	struct OBB {
		const D3DXVECTOR3 *ext;
		const D3DXMATRIX *trans;

		D3DXVECTOR3 getAxis(int axis) {
			return matrixGetRow(*trans,axis);
		}
	};

	OBB obb0,obb1;
	obb0.ext = boxext0;
	obb0.trans = trans0;
	obb1.ext = boxext1;
	obb1.trans = trans1;

	auto cross = [](const D3DXVECTOR3 &v0,const D3DXVECTOR3 &v1)->D3DXVECTOR3 {
		D3DXVECTOR3 ret;
		D3DXVec3Cross(&ret,&v0,&v1);
		return ret;
	};

	auto transformToAxis = [&](OBB &obb,const D3DXVECTOR3 &axis)->float {
		return obb.ext->x * abs(D3DXVec3Dot(&axis,&obb.getAxis(0))) +
			obb.ext->y * abs(D3DXVec3Dot(&axis,&obb.getAxis(1))) +
			obb.ext->z * abs(D3DXVec3Dot(&axis,&obb.getAxis(2)));
	};

	auto overlapOnAxis = [&](OBB &obb0,OBB &obb1,const D3DXVECTOR3 &axis,const D3DXVECTOR3 &tocenter)->float {
		float proj[2];

		proj[0] = transformToAxis(obb0,axis);
		proj[1] = transformToAxis(obb1,axis);
		float distance = abs(D3DXVec3Dot(&tocenter,&axis));

		return proj[0] + proj[1] - distance;
	};

	struct TEST {
		D3DXVECTOR3 axis;
		float overlap;
	};

	TEST tests[15] = {
		{obb0.getAxis(0),0},
		{obb0.getAxis(1),0},
		{obb0.getAxis(2),0},

		{obb1.getAxis(0),0},
		{obb1.getAxis(1),0},
		{obb1.getAxis(2),0},

		{cross(obb0.getAxis(0),obb1.getAxis(0)),0},
		{cross(obb0.getAxis(0),obb1.getAxis(1)),0},
		{cross(obb0.getAxis(0),obb1.getAxis(2)),0},
		{cross(obb0.getAxis(1),obb1.getAxis(0)),0},
		{cross(obb0.getAxis(1),obb1.getAxis(1)),0},
		{cross(obb0.getAxis(1),obb1.getAxis(2)),0},
		{cross(obb0.getAxis(2),obb1.getAxis(0)),0},
		{cross(obb0.getAxis(2),obb1.getAxis(1)),0},
		{cross(obb0.getAxis(2),obb1.getAxis(2)),0},
	};

	D3DXVECTOR3 tocenter = obb1.getAxis(3) - obb0.getAxis(3);

	int best = 0,bestsingle = 0;
	for(int i = 0; i < 15; i++) {
		if(D3DXVec3LengthSq(&tests[i].axis) < 0.0001f) continue;
		D3DXVec3Normalize(&tests[i].axis,&tests[i].axis);
		tests[i].overlap = overlapOnAxis(obb0,obb1,tests[i].axis,tocenter);
		if(tests[i].overlap < 0) return false; // no collision if there is a separating axis
		if(tests[i].overlap < tests[best].overlap) {
			if(i < 6) bestsingle = i;
			best = i;
		}
	}

	return true;
}