#include "hdx9ui.h"

class HDXTexture;
class HDXUIElementEffect_TextureDraw;
class HDXUIElementEffect_TextDraw;

class HDXUIElement_Checkbox : public HDXUIElementBase {
public:
	HDXUIElement_Checkbox(const std::string &name,
						  HDXUIElementBase *parent,
						  const std::string &maintexture,
						  const std::string &hovertexture,
						  const std::string &checktexture,
						  const std::string &textfont,
						  const std::string &text,
						  DWORD textflags,
						  D3DXCOLOR textcolor);
	virtual ~HDXUIElement_Checkbox();

	HDXUIElementEffect_TextureDraw* getMainTextureEffect() const;
	HDXUIElementEffect_TextureDraw* getHoverTextureEffect() const;
	HDXUIElementEffect_TextureDraw* getCheckTextureEffect() const;

	HDXUIElementEffect_TextDraw* getTextEffect() const;

	bool getChecked() const;
	void setChecked(bool checked);

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransition);

	virtual void update(float dt);

private:
	HDXUIElementEffect_TextureDraw *meffect_texture_main,*meffect_texture_hover,*meffect_texture_check;
	HDXUIElementEffect_TextDraw *meffect_text;
	bool mchecked;
	int mcallbackid_clicked;
};
