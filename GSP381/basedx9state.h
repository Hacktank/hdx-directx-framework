#pragma once

#include "hdx9.h"

class BaseDX9State {
public:
	BaseDX9State() {}
	virtual ~BaseDX9State() {}

	virtual void stateEnter() = 0;
	virtual void stateExecute(float dt) = 0;
	virtual void stateDraw() = 0;
	virtual void stateExit() = 0;
	virtual void stateOnLostDevice() = 0;
	virtual void stateOnResetDevice() = 0;
};
