
#include "hdx9shader_base.h"
#include "hdx9.h"

#include "hdx9texture.h"


HDXShader_Base::HDXShader_Base(IDirect3DDevice9 *device,const char *fname,const char *techname) {
	mdevice = device;

	ID3DXBuffer *errors = 0;
	D3DXCreateEffectFromFile(mdevice,fname,0,0,
#ifdef _DEBUG
							 D3DXSHADER_DEBUG,
#else
							 0,
#endif
							 0,&mfx,&errors);

	if(errors) {
		MessageBox(0,(char*)errors->GetBufferPointer(),0,0);
		mfx = 0;
	} else if(mfx!=0) {
		setTechnique(techname);

		mhandle_wvp = mfx->GetParameterByName(0,"g_wvp");
		mhandle_texture = mfx->GetParameterByName(0,"g_tex");
		mhandle_texture_enabled = mfx->GetParameterByName(0,"g_tex_enabled");

		mhandle_material_diffuse = mfx->GetParameterByName(0,"g_mat_diffuse");
		mhandle_material_ambient = mfx->GetParameterByName(0,"g_mat_ambient");
		mhandle_material_emissive = mfx->GetParameterByName(0,"g_mat_emissive");
		mhandle_material_specular = mfx->GetParameterByName(0,"g_mat_specular");
		mhandle_material_power = mfx->GetParameterByName(0,"g_mat_power");

		setWVPFromDevice();
		setTexture(0);

		setMaterial(HDXMAT_WHITE);

		mcbid_ondevicelost = HDX_MAIN->registerFuncOnDeviceLost([&]()->void {
			mfx->OnLostDevice();
		});

		mcbid_ondevicereset = HDX_MAIN->registerFuncOnDeviceReset([&]()->void {
			mfx->OnResetDevice();
		});
	}
}

HDXShader_Base::~HDXShader_Base() {
	if(mfx) {
		mfx->Release();
		HDX_MAIN->unregisterFuncOnDeviceLost(mcbid_ondevicelost);
		HDX_MAIN->unregisterFuncOnDeviceReset(mcbid_ondevicereset);
	}
}

ID3DXEffect* HDXShader_Base::getD3DEffect() const {
	return mfx;
}

IDirect3DDevice9* HDXShader_Base::getD3DDevice() const {
	return mdevice;
}

void HDXShader_Base::setTechnique(const char *name) {
	if(mfx) {
		mhandle_tech = mfx->GetTechniqueByName(name);
	}
}

void HDXShader_Base::setMatrixWorld(const D3DXMATRIX &mat) {
	mmat_w = mat;
	if(mfx) {
		mfx->SetMatrix(mhandle_wvp,&(mmat_w*mmat_v*mmat_p));
		mfx->CommitChanges();
	}
}

D3DXMATRIX HDXShader_Base::getMatrixWorld() const {
	return mmat_w;
}

void HDXShader_Base::setMatrixView(const D3DXMATRIX &mat) {
	mmat_v = mat;
	if(mfx) {
		mfx->SetMatrix(mhandle_wvp,&(mmat_w*mmat_v*mmat_p));
		mfx->CommitChanges();
	}
}

D3DXMATRIX HDXShader_Base::getMatrixView() const {
	return mmat_v;
}

void HDXShader_Base::setMatrixProjection(const D3DXMATRIX &mat) {
	mmat_p = mat;
	if(mfx) {
		mfx->SetMatrix(mhandle_wvp,&(mmat_w*mmat_v*mmat_p));
		mfx->CommitChanges();
	}
}

D3DXMATRIX HDXShader_Base::getMatrixProjection() const {
	return mmat_p;
}

void HDXShader_Base::setTexture(HDXTexture *tex) {
	mtex = tex;

	IDirect3DTexture9 *d3dtex = 0;
	if(tex) d3dtex = tex->getD3DTexture();
	if(mdevice) {
		mdevice->SetTexture(0,d3dtex);
	}
	if(mfx) {
		mfx->SetBool(mhandle_texture_enabled,d3dtex!=0);
		mfx->SetTexture(mhandle_texture,d3dtex);
		mfx->CommitChanges();
	}
}

HDXTexture* HDXShader_Base::getTexture() const {
	return mtex;
}

void HDXShader_Base::setMaterial(const HDXMaterial &mat) {
	mmaterial_cur = mat;
	if(mfx) {
		mfx->SetFloatArray(mhandle_material_ambient,(float*)&mmaterial_cur.ambient,4);
		mfx->SetFloatArray(mhandle_material_diffuse,(float*)&mmaterial_cur.diffuse,4);
		mfx->SetFloatArray(mhandle_material_specular,(float*)&mmaterial_cur.specular,4);
		mfx->SetFloatArray(mhandle_material_emissive,(float*)&mmaterial_cur.emisive,4);
		mfx->SetFloat(mhandle_material_power,mmaterial_cur.power);
		mfx->CommitChanges();
	}
}

HDXMaterial HDXShader_Base::getMaterial() const {
	return mmaterial_cur;
}

void HDXShader_Base::setWVPFromDevice() {
	D3DXMATRIX w,v,p;

	mdevice->GetTransform(D3DTS_WORLD,&w);
	mdevice->GetTransform(D3DTS_VIEW,&v);
	mdevice->GetTransform(D3DTS_PROJECTION,&p);

	setMatrixWorld(w);
	setMatrixView(v);
	setMatrixProjection(p);
}

void HDXShader_Base::pushMatrixWorld() {
	mmat_world_stack.push(getMatrixWorld());
}

void HDXShader_Base::popMatrixWorld() {
	setMatrixWorld(mmat_world_stack.top());
	mmat_world_stack.pop();
}

void HDXShader_Base::multMatrixWorld(const D3DXMATRIX &mat) {
	setMatrixWorld(getMatrixWorld() * mat);
}

void HDXShader_Base::doDraw(std::function<void(HDXShader_Base *hdxfx)> func) {
	if(mfx) {
		UINT numpasses = 0;
		mfx->Begin(&numpasses,0);
		for(UINT i = 0; i < numpasses; ++i) {
			mfx->BeginPass(i);
			func(this);
			mfx->EndPass();
		}
		mfx->End();
	}
}
