
#pragma once

#include <d3d9.h>
#include <d3dx9.h>

class HDXCamera {
public:
	HDXCamera();
	virtual ~HDXCamera();

	virtual D3DXVECTOR3 getPosition() const;
	virtual void setPosition(const D3DXVECTOR3 &pos);
	virtual void setPosition(float x,float y,float z);

	virtual D3DXQUATERNION getOrientation() const;
	virtual void setOrientation(const D3DXQUATERNION &ori);

	virtual void translate(const D3DXVECTOR3 &v);
	virtual void rotateAxis(const D3DXVECTOR3 &axis,float a);
	//y axis
	virtual void rotateYaw(float a);
	//x axis
	virtual void rotatePitch(float a);
	//z axis
	virtual void rotateRoll(float a);

	virtual D3DXVECTOR3 getForward() const;
	virtual D3DXVECTOR3 getRight() const;
	virtual D3DXVECTOR3 getUp() const;

	virtual void setLookAt(const D3DXVECTOR3 &point);
	virtual void setLookDirection(const D3DXVECTOR3 &dir);

	virtual D3DXMATRIX getViewMatrix() const;

private:
	D3DXMATRIX mview;

	D3DXVECTOR3 mpos;
	D3DXQUATERNION mori;
	bool mchanged;
};
