#pragma once

#include "basedx9state.h"
#include "basemanager.h"

#include "hdx9sound.h"
#include "hdx9mesh.h"
#include "hdx9shader_phonglighting.h"
#include "hdx9light.h"

#include "messagelistener.h"

#define SSG_DIR_FORWARD (ZUNIT3)
#define SSG_DIR_UP (YUNIT3)
#define SSG_DIR_RIGHT (XUNIT3)

class State_SpaceGame;

enum {
	SSG_ENTTYPE_PLAYER = 0,
	SSG_ENTTYPE_ENEMY,
	SSG_ENTTYPE_DEBRIS,
	SSG_ENTTYPE_ASTEROID,
	SSG_ENTTYPE_PICKUP_SPEEDBOOST
};

class State_SpaceGame_Entity_Base : public MessageListener {
public:
	int mid;
	int mtype;

	State_SpaceGame *mparent;

	bool mactive;

	D3DXVECTOR3 mpos;
	D3DXVECTOR3 mvel;
	D3DXQUATERNION mori;
	D3DXVECTOR3 mavel;
	D3DXVECTOR3 mcolext;

	D3DXMATRIX mtrans;
	D3DXMATRIX mbasetrans;

	HDXMesh *mmesh;

	State_SpaceGame_Entity_Base(int id,State_SpaceGame *parent);
	virtual ~State_SpaceGame_Entity_Base();

	virtual void update(float dt);
	virtual void draw(HDXShader_PhongLighting *shader);

	void setMesh(HDXMesh *mesh,const D3DXMATRIX &basetransform);

	void recalculateTransformMatrix();

	void markForDeletion();
};

#define SSG_ENT_PLAYER_SPEED_START 30.0f
#define SSG_ENT_PLAYER_SPEED_BOOSTEFFECT 15.0f
#define SSG_ENT_PLAYER_SPEED_IMPACTEFFECT 5.0f;
#define SSG_ENT_PLAYER_SPEED_MAX_PLANAR 30.0f
#define SSG_ENT_PLAYER_SPEED_DAMPEN 2.0f
#define SSG_ENT_PLAYER_VEL_ACCELERATION (SSG_ENT_PLAYER_SPEED_MAX_PLANAR*1.75f)
#define SSG_ENT_PLAYER_CAMERA_POS_DAMPEN 4.0f
#define SSG_ENT_PLAYER_POS_DAMPEN 0.8f
#define SSG_ENT_PLAYER_MAXHEALTH 3
class State_SpaceGame_Entity_Player : public State_SpaceGame_Entity_Base {
public:
	enum {
		SSG_PC_U = 0,
		SSG_PC_L,
		SSG_PC_D,
		SSG_PC_R,

		SSG_PC_NUM
	};

	int mcbid_onkeypress;
	int mcbid_onkeyrelease;

	bool mcontrol_pressed[SSG_PC_NUM];
	char mcontrol_key[SSG_PC_NUM];

	float mmaxspeed_planar;

	int mhealth;

	HDXLight *mlight_headlight;

	HDX9Sound *msound_hum;
	FMOD::Channel *msound_hum_channel;

	HDX9Sound *msound_powerup;
	std::vector<HDX9Sound*> msound_impacts;

	State_SpaceGame_Entity_Player(int id,State_SpaceGame *parent);
	virtual ~State_SpaceGame_Entity_Player();

	virtual void update(float dt);
	virtual void draw(HDXShader_PhongLighting *shader);

	D3DXVECTOR3 _getDesiredCameraPosition();
};

#define SSG_ENT_ENEMY_SPEED_START 60.0f
#define SSG_ENT_ENEMY_DROPRATE_MIN 0.1f
#define SSG_ENT_ENEMY_DROPRATE_MAX 0.5f
class State_SpaceGame_Entity_Enemy : public State_SpaceGame_Entity_Base {
public:
	float mdroptimer;

	State_SpaceGame_Entity_Enemy(int id,State_SpaceGame *parent);
	virtual ~State_SpaceGame_Entity_Enemy();

	virtual void update(float dt);
	virtual void draw(HDXShader_PhongLighting *shader);
};

class State_SpaceGame_Entity_Debris : public State_SpaceGame_Entity_Base {
public:
	State_SpaceGame_Entity_Debris(int id,State_SpaceGame *parent);
	virtual ~State_SpaceGame_Entity_Debris();

	virtual void update(float dt);
	virtual void draw(HDXShader_PhongLighting *shader);
};

class State_SpaceGame_Entity_Asteroid : public State_SpaceGame_Entity_Debris {
public:
	HDXTexture *mtexture;

	State_SpaceGame_Entity_Asteroid(int id,State_SpaceGame *parent);
	virtual ~State_SpaceGame_Entity_Asteroid();

	virtual void update(float dt);
	virtual void draw(HDXShader_PhongLighting *shader);
};

#define SSG_ENT_PICKUP_SPEEDBOOST_EFFECTTIME 7000
class State_SpaceGame_Entity_Pickup_SpeedBoost : public State_SpaceGame_Entity_Debris {
public:
	HDXLight *mlight_point;

	State_SpaceGame_Entity_Pickup_SpeedBoost(int id,State_SpaceGame *parent);
	virtual ~State_SpaceGame_Entity_Pickup_SpeedBoost();

	virtual void update(float dt);
};

enum SSG_GAMESTATE {
	SSG_GS_NONE = 0,
	SSG_GS_PLAYING,
	SSG_GS_WIN,
	SSG_GS_LOSE
};

class State_SpaceGame : public BaseDX9State,public MessageListener {
public:
	State_SpaceGame() {}
	virtual ~State_SpaceGame() {}

	virtual void stateEnter();
	virtual void stateExecute(float dt);
	virtual void stateDraw();
	virtual void stateExit();
	virtual void stateOnLostDevice();
	virtual void stateOnResetDevice();

	HDXShader_PhongLighting* getShader();

	std::vector<HDXMesh*>* getPreloadedMeshes();
	std::vector<HDXTexture*>* getPreloadedTextures();

	State_SpaceGame_Entity_Player* getPlayer();
	State_SpaceGame_Entity_Enemy* getEnemy();

	SSG_GAMESTATE getGameState();

	template<typename ENTT>
	int createEntity();
	State_SpaceGame_Entity_Base* getEntity(int id);
	void destroyEntity(int id);
	void destroyAllEntities();

private:
	HDXShader_PhongLighting *mhdxfx;

	BaseManagerID<State_SpaceGame_Entity_Base*> mentities;
	int mentid_player,mentid_enemy;

	SSG_GAMESTATE mgamestate;

	HDXLight *mlight_directional;

	std::vector<HDXMesh*> mpreload_mesh;
	std::vector<HDXTexture*> mpreload_texture;

	int mmsgregid_entitydelete;
	int mmsgregid_gamereset;
	int mmsgregid_ongamewin;
	int mmsgregid_ongamelose;

	int mcbid_onkeypress;
};

template<typename ENTT>
int State_SpaceGame::createEntity() {
	ENTT *ent = new ENTT(mentities.getNVID(),this);
	mentities.addItem(ent);
	return ent->mid;
}
