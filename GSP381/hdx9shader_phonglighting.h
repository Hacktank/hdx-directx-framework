
#pragma once

#include "hdx9shader_base.h"

#include "basemanager.h"
#include "hdx9light.h"

class HDXShader_PhongLighting : public HDXShader_Base {
public:
	HDXShader_PhongLighting(IDirect3DDevice9 *device,const char *fname,const char *techname);
	virtual ~HDXShader_PhongLighting();

	virtual void setMatrixWorld(const D3DXMATRIX &mat);

	virtual void setMatrixView(const D3DXMATRIX &mat);

	virtual void doDraw(std::function<void(HDXShader_Base *hdxfx)> func);

	HDXLight* createLight();
	HDXLight* getLight(int id);
	void destroyLight(int id);

private:
	unsigned int mlight_maxnum;

	BaseManagerID<HDXLight*> mlights;

	D3DXHANDLE mhandle_mat_world;
	D3DXHANDLE mhandle_mat_world_inv_trans;
	D3DXHANDLE mhandle_eye_pos;

	D3DXHANDLE mhandle_light_num;
	D3DXHANDLE mhandle_light_type;
	D3DXHANDLE mhandle_light_ambient;
	D3DXHANDLE mhandle_light_diffuse;
	D3DXHANDLE mhandle_light_specular;
	D3DXHANDLE mhandle_light_pos_w;
	D3DXHANDLE mhandle_light_attenuation_012;
	D3DXHANDLE mhandle_light_direction;
	D3DXHANDLE mhandle_light_falloff;
	D3DXHANDLE mhandle_light_theta;
	D3DXHANDLE mhandle_light_phi;
};
