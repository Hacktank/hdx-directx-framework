
#pragma once

#include <d3d9.h>
#include <d3dx9.h>

enum {
	HDXLT_DIRECTIONAL = 0,
	HDXLT_POINT,
	HDXLT_SPOT,

	HDXLT_NUM
};

class HDXLight {
public:
	HDXLight(int id);
	~HDXLight();

	int getType() const;

	int getID() const;

	bool isEnabled() const;
	void setEnabled(bool enabled);

	D3DXCOLOR getColorAmbient() const;
	void setColorAmbient(const D3DXCOLOR &col);

	D3DXCOLOR getColorDiffuse() const;
	void setColorDiffuse(const D3DXCOLOR &col);

	D3DXCOLOR getColorSpecular() const;
	void setColorSpecular(const D3DXCOLOR &col);

	D3DXVECTOR3 getPosition() const;
	void setPosition(const D3DXVECTOR3 &v);

	D3DXVECTOR3 getDirection() const;
	void setDirection(const D3DXVECTOR3 &v);

	D3DXVECTOR3 getAttenuation() const;
	void setAttenuation(const D3DXVECTOR3 &v);

	float getFalloff() const;
	void setFalloff(float falloff);

	float getTheta() const;
	void setTheta(float theta);

	float getPhi() const;
	void setPhi(float phi);

	void setLightDirectional(const D3DXCOLOR &ambient,
							 const D3DXCOLOR &diffuse,
							 const D3DXCOLOR &specular,
							 const D3DXVECTOR3 &direction);

	void setLightPoint(const D3DXCOLOR &ambient,
					   const D3DXCOLOR &diffuse,
					   const D3DXCOLOR &specular,
					   const D3DXVECTOR3 &pos,
					   const D3DXVECTOR3 &attenuation);

	void setLightSpot(const D3DXCOLOR &ambient,
					  const D3DXCOLOR &diffuse,
					  const D3DXCOLOR &specular,
					  const D3DXVECTOR3 &pos,
					  const D3DXVECTOR3 &attenuation,
					  const D3DXVECTOR3 &direction,
					  float falloff,
					  float theta,
					  float phi);

private:
	int mtype;
	int mid;
	bool menabled;

	D3DXCOLOR mcol_ambient,mcol_diffuse,mcol_specular;
	D3DXVECTOR3 mpos;
	D3DXVECTOR3 mattenuation;
	D3DXVECTOR3 mdirection;
	float mfalloff;
	float mtheta;
	float mphi;
};