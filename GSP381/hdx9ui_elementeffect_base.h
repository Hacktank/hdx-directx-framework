#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <directxmath.h>
#include <string>

#include "hdx9ui_imenueventhandler.h"

class HDXUIElementBase;

class HDXUIElementEffect_Base : virtual public IHDXMenuEventHandler {
public:
	HDXUIElementEffect_Base(const std::string &name,HDXUIElementBase *parent);
	virtual ~HDXUIElementEffect_Base();

	std::string getName() const;

	HDXUIElementBase* getParent() const;

	bool isActive() const;
	void setActive(bool active);

	bool isEnabled() const;
	void setEnabled(bool enabled);

	virtual float getZPos() const; //returns parent's zpos by default

	virtual float getDesiredTransitionTime() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransiton);

	virtual void update(float dt);
	virtual void render();

private:
	std::string mname;
	HDXUIElementBase *mparent;
	bool mactive,menabled;
};
