#include "state_spacegame.h"

#include "hdx9.h"
#include "hdx9font.h"
#include "hdx9texture.h"
#include "hdx9inputmanager.h"
#include "hdx9musicmanager.h"

#include "hdx9ui.h"

#include "hdx9color.h"
#include "hdx9vertex.h"
#include "hdx9mesh.h"

#include "hdx9intersection.h"
#include "hdx9math.h"
#include <algorithm>

#include <sstream>

State_SpaceGame_Entity_Base::State_SpaceGame_Entity_Base(int id,State_SpaceGame *parent) {
	mtype = -1;
	mid = id;
	mparent = parent;

	mactive = true;

	mpos.x = mpos.y = mpos.z = 0.0f;
	mvel.x = mvel.y = mvel.z = 0.0f;
	mavel.x = mavel.y = mavel.z = 0.0f;
	mori.x = mori.y = mori.z = 0.0f; mori.w = 1.0f;
	mcolext.x = mcolext.y = mcolext.z = 0.0f;

	D3DXMatrixIdentity(&mtrans);
	D3DXMatrixIdentity(&mbasetrans);

	mmesh = 0;
}

State_SpaceGame_Entity_Base::~State_SpaceGame_Entity_Base() {
	if(mmesh) mmesh->release();
}

void State_SpaceGame_Entity_Base::update(float dt) {
	mpos += mvel*dt;

	{
		D3DXQUATERNION q(-mavel.x,-mavel.y,-mavel.z,0.0f);
		mori += ((q*dt) * mori)*0.5f;
		D3DXQuaternionNormalize(&mori,&mori);
	}

	recalculateTransformMatrix();
}

void State_SpaceGame_Entity_Base::draw(HDXShader_PhongLighting *shader) {
	IDirect3DDevice9 *device = shader->getD3DDevice();

	if(device && mmesh) {
		shader->pushMatrixWorld();

		shader->multMatrixWorld(mtrans);

		for(unsigned int i = 0; i < mmesh->mmaterials.size(); i++) {
			shader->setMaterial(mmesh->mmaterials[i]);
			shader->setTexture(mmesh->mtextures[i]);
			mmesh->md3dmesh->DrawSubset(i);
		}

		shader->popMatrixWorld();
	}
}

void State_SpaceGame_Entity_Base::setMesh(HDXMesh *mesh,const D3DXMATRIX &basetransform) {
	mmesh = mesh;
	mbasetrans = basetransform;

	recalculateTransformMatrix();

	//calculate collision extents
	if(mmesh) {
		mcolext.x = mcolext.y = mcolext.z = 0.0f;

		IDirect3DVertexBuffer9 *vb;
		mmesh->md3dmesh->GetVertexBuffer(&vb);

		HDXVERTPCTN *verts;
		vb->Lock(0,mmesh->md3dmesh->GetNumVertices()*sizeof(HDXVERTPCTN),(void**)&verts,0);

		for(unsigned int i = 0; i < mmesh->md3dmesh->GetNumVertices(); i++) {
			for(int j = 0; j < 3; j++) {
				mcolext[j] = (std::max)(mcolext[j],fabs(verts[i].p[j]));
			}
		}

		vb->Unlock();

		mcolext *= 0.75f; //make the collision volume significantly smaller than the mesh
	}
}

void State_SpaceGame_Entity_Base::recalculateTransformMatrix() {
	D3DXMATRIX mat_rot;
	D3DXMATRIX mat_trans;

	D3DXMatrixRotationQuaternion(&mat_rot,&mori);
	D3DXMatrixTranslation(&mat_trans,mpos.x,mpos.y,mpos.z);

	mtrans = mbasetrans * mat_rot * mat_trans;
}

void State_SpaceGame_Entity_Base::markForDeletion() {
	mactive = false;
	MSGMAN->enqueueMessage(0,mid,"state_SpaceGame:deleteEntity",mid);
}

State_SpaceGame_Entity_Player::State_SpaceGame_Entity_Player(int id,State_SpaceGame *parent) : State_SpaceGame_Entity_Base(id,parent) {
	mtype = SSG_ENTTYPE_PLAYER;

	mcontrol_key[SSG_PC_U] = 'W';
	mcontrol_key[SSG_PC_L] = 'A';
	mcontrol_key[SSG_PC_D] = 'S';
	mcontrol_key[SSG_PC_R] = 'D';

	for(int i = 0; i < SSG_PC_NUM; i++) {
		mcontrol_pressed[i] = false;
	}

	mcbid_onkeypress = HDX_INPUT->registerCallbackOnKeyPress([&](WPARAM key) {
		for(int i = 0; i < SSG_PC_NUM; i++) {
			if(key == mcontrol_key[i]) mcontrol_pressed[i] = true;
		}
	});

	mcbid_onkeyrelease = HDX_INPUT->registerCallbackOnKeyRelease([&](WPARAM key,float timeheld) {
		for(int i = 0; i < SSG_PC_NUM; i++) {
			if(key == mcontrol_key[i]) mcontrol_pressed[i] = false;
		}
	});

	mmaxspeed_planar = SSG_ENT_PLAYER_SPEED_MAX_PLANAR;

	{
		D3DXMATRIX basetrans;
		D3DXMatrixRotationQuaternion(&basetrans,&quaterionRotateTo(XUNIT3,SSG_DIR_FORWARD));

		HDXMesh *mesh = HDXMeshLoadFromX("data/mesh/spacechase/player_ship.x","data/texture/spacechase/player_ship/",true);

		setMesh(mesh,basetrans);

		//original mesh has no ambient or specular
		if(mmesh) {
			for(unsigned int i = 0; i < mmesh->mmaterials.size(); i++) {
				mmesh->mmaterials[i].ambient = HDXCOL_WHITE*0.5f;
				mmesh->mmaterials[i].specular = HDXCOL_WHITE*0.5f;
				mmesh->mmaterials[i].power = 10;
			}
		}
	}

	HDX_MAIN->getCameraCurrent()->setPosition(_getDesiredCameraPosition());

	mvel += SSG_DIR_FORWARD * SSG_ENT_PLAYER_SPEED_START;

	mlight_headlight = mparent->getShader()->createLight();
	mlight_headlight->setLightSpot(HDXCOL_BLACK,
								   HDXCOL_WHITE*0.8f,
								   HDXCOL_WHITE*0.5f,
								   D3DXVECTOR3(0,0,0),
								   D3DXVECTOR3(0,0,0.005f),
								   SSG_DIR_FORWARD,
								   10.0f,
								   (float)M_PI*0.4f*0.6f,
								   (float)M_PI*0.4f);
	mlight_headlight->setEnabled(true);

	addMessageSubscription(mid,"state_SpaceGame:player:onDamage",MSGCBFROMLAM(this,int amount) {
		mhealth -= amount;
		if(mhealth <= 0) {
			MSGMAN->enqueueMessage(0,mid,"state_SpaceGame:onGameLose");
		}
	});

	addMessageSubscription(mid,"state_SpaceGame:onCollision",MSGCBFROMLAM(this,int otherid) {
		State_SpaceGame_Entity_Base *ent_other_base = mparent->getEntity(otherid);
		if(ent_other_base) {
			switch(ent_other_base->mtype) {
				case SSG_ENTTYPE_PLAYER: {
					break;
				}
				case SSG_ENTTYPE_ENEMY: {
					MSGMAN->enqueueMessage(0,mid,"state_SpaceGame:onGameWin");
					ent_other_base->markForDeletion();
					break;
				}
				case SSG_ENTTYPE_ASTEROID: {
					mvel -= SSG_DIR_FORWARD * SSG_ENT_PLAYER_SPEED_IMPACTEFFECT;

					HDX9Sound *snd_impact_hdx = msound_impacts[rand()%msound_impacts.size()];
					auto snd_impact_channel = HDX_SOUNDMAN->playSoundChannelGroup(snd_impact_hdx,"fx",true);
					FMOD_VECTOR fmv_pos = {
						ent_other_base->mpos.x,
						ent_other_base->mpos.y,
						ent_other_base->mpos.z,
					};
					FMOD_VECTOR fmv_vel = {
						ent_other_base->mvel.x,
						ent_other_base->mvel.y,
						ent_other_base->mvel.z,
					};
					snd_impact_channel->set3DAttributes(&fmv_pos,&fmv_vel);
					snd_impact_channel->setPaused(false);

					ent_other_base->markForDeletion();

					MSGMAN->enqueueMessage(0,mid,"state_SpaceGame:player:onDamage",1);
					break;
				}
				case SSG_ENTTYPE_PICKUP_SPEEDBOOST: {
					mvel += SSG_DIR_FORWARD * SSG_ENT_PLAYER_SPEED_BOOSTEFFECT;
					mmaxspeed_planar += SSG_ENT_PLAYER_SPEED_BOOSTEFFECT;

					HDX_SOUNDMAN->playSoundChannelGroup(msound_powerup,"fx");

					MSGMAN->enqueueMessage(SSG_ENT_PICKUP_SPEEDBOOST_EFFECTTIME,mid,"state_Spacegame:boostTimeout:speed");
					ent_other_base->markForDeletion();
					break;
				}
			}
		}
	});

	addMessageSubscription(mid,"state_Spacegame:boostTimeout:speed",MSGCBFROMLAM(this,void) {
		mvel -= SSG_DIR_FORWARD * (SSG_ENT_PLAYER_SPEED_BOOSTEFFECT*0.8f); // don't remove all the gained speed
		mmaxspeed_planar -= (SSG_ENT_PLAYER_SPEED_BOOSTEFFECT*0.8f);
	});

	mhealth = SSG_ENT_PLAYER_MAXHEALTH;

	msound_impacts.push_back(HDX_SOUNDMAN->loadSoundFAF("data/sound/spacechase/impact_0.wav",FMOD_DEFAULT|FMOD_3D));
	msound_impacts.push_back(HDX_SOUNDMAN->loadSoundFAF("data/sound/spacechase/impact_1.wav",FMOD_DEFAULT|FMOD_3D));
	msound_impacts.push_back(HDX_SOUNDMAN->loadSoundFAF("data/sound/spacechase/impact_2.wav",FMOD_DEFAULT|FMOD_3D));
	msound_impacts.push_back(HDX_SOUNDMAN->loadSoundFAF("data/sound/spacechase/impact_3.wav",FMOD_DEFAULT|FMOD_3D));
	msound_powerup = HDX_SOUNDMAN->loadSoundFAF("data/sound/spacechase/powerup.wav",FMOD_DEFAULT);

	msound_hum = HDX_SOUNDMAN->loadSoundStream("data/sound/spacechase/ship_hum.wav",FMOD_DEFAULT);
	msound_hum_channel = HDX_SOUNDMAN->playSoundChannelGroup(msound_hum,"fx",false,-1);
}

State_SpaceGame_Entity_Player::~State_SpaceGame_Entity_Player() {
	HDX_INPUT->unregisterCallbackOnKeyPress(mcbid_onkeypress);
	HDX_INPUT->unregisterCallbackOnKeyRelease(mcbid_onkeyrelease);

	mparent->getShader()->destroyLight(mlight_headlight->getID());

	msound_hum_channel->stop();
	msound_hum->release();
	msound_powerup->release();

	for(unsigned int i = 0; i < msound_impacts.size(); i++) {
		msound_impacts[i]->release();
	}
}

void State_SpaceGame_Entity_Player::update(float dt) {
	State_SpaceGame_Entity_Base::update(dt);

	FMOD::System *fmodsys = HDX_SOUNDMAN->getFMODSystem();

	if(fmodsys) {
		FMOD_VECTOR fm_pos,fm_vel,fm_forw,fm_up;
		fm_pos.x = mpos.x;
		fm_pos.y = mpos.y;
		fm_pos.z = mpos.z;

		fm_vel.x = mvel.x;
		fm_vel.y = mvel.y;
		fm_vel.z = mvel.z;

		{
			D3DXVECTOR3 dir = matrixGetRow(mtrans,3);
			fm_forw.x = dir.x;
			fm_forw.y = dir.y;
			fm_forw.z = dir.z;
		}

		{
			D3DXVECTOR3 dir = matrixGetRow(mtrans,2);
			fm_up.x = dir.x;
			fm_up.y = dir.y;
			fm_up.z = dir.z;
		}

		fmodsys->set3DListenerAttributes(0,&fm_pos,&fm_vel,&fm_forw,&fm_up);
	}

	HDXCamera *cam = HDX_MAIN->getCameraCurrent();

	{
		D3DXVECTOR3 cam_tpos = _getDesiredCameraPosition();
		D3DXVECTOR3 cam_pos = cam->getPosition();

		const D3DXVECTOR3 cam_pos_delta = (cam_tpos-cam_pos) * (SSG_ENT_PLAYER_CAMERA_POS_DAMPEN*dt);

		cam_pos += cam_pos_delta;

		cam->setPosition(cam_pos);
		cam->setLookAt(mpos + (SSG_DIR_UP*mcolext.y) + (-SSG_DIR_FORWARD*mcolext.x*0.5f));
	}

	{
		mlight_headlight->setPosition(mpos + (SSG_DIR_FORWARD*(mcolext.x+0.5f)));
	}

	if(mparent->getGameState() == SSG_GS_PLAYING) {
		if(mcontrol_pressed[SSG_PC_U]) {
			mvel += SSG_DIR_UP * (mmaxspeed_planar*1.75f)* dt;
		}

		if(mcontrol_pressed[SSG_PC_L]) {
			mvel += -SSG_DIR_RIGHT * (mmaxspeed_planar*1.75f) * dt;
		}

		if(mcontrol_pressed[SSG_PC_D]) {
			mvel += -SSG_DIR_UP * (mmaxspeed_planar*1.75f) * dt;
		}

		if(mcontrol_pressed[SSG_PC_R]) {
			mvel += SSG_DIR_RIGHT * (mmaxspeed_planar*1.75f) * dt;
		}
	}

	//lerp position towards the forward axis
	{
		D3DXVECTOR3 tpos(0,0,0);
		tpos += SSG_DIR_FORWARD * D3DXVec3Dot(&SSG_DIR_FORWARD,&mpos);
		mpos = mpos + ((tpos-mpos) * (SSG_ENT_PLAYER_POS_DAMPEN*dt));
	}

	//dampen planar speed
	{
		auto lam_dampenVelDir = [&](const D3DXVECTOR3 &dir) {
			const float speed_dir = D3DXVec3Dot(&dir,&mvel);
			float speed_delta = speed_dir*-(SSG_ENT_PLAYER_SPEED_DAMPEN*dt);

			mvel += dir * speed_delta;
		};

		if(!mcontrol_pressed[SSG_PC_U] && !mcontrol_pressed[SSG_PC_D]) {
			lam_dampenVelDir(SSG_DIR_UP);
		}

		if(!mcontrol_pressed[SSG_PC_L] && !mcontrol_pressed[SSG_PC_R]) {
			lam_dampenVelDir(SSG_DIR_RIGHT);
		}
	}

	//clamp planar speed
	{
		D3DXVECTOR3 planarvel =
			D3DXVec3Dot(&SSG_DIR_RIGHT,&mvel)*SSG_DIR_RIGHT +
			D3DXVec3Dot(&SSG_DIR_UP,&mvel)*SSG_DIR_UP;

		float finalplanarspeed = (std::min)(mmaxspeed_planar,D3DXVec3Length(&planarvel));
		D3DXVec3Normalize(&planarvel,&planarvel);
		planarvel *= finalplanarspeed;

		mvel = D3DXVec3Dot(&SSG_DIR_FORWARD,&mvel)*SSG_DIR_FORWARD + planarvel;
	}
}

void State_SpaceGame_Entity_Player::draw(HDXShader_PhongLighting *shader) {
	State_SpaceGame_Entity_Base::draw(shader);
}

D3DXVECTOR3 State_SpaceGame_Entity_Player::_getDesiredCameraPosition() {
	D3DXVECTOR4 cam_tpos_4;

	switch(mparent->getGameState()) {
		case SSG_GS_PLAYING: {
			D3DXVec3Transform(&cam_tpos_4,&D3DXVECTOR3(-mcolext.x*2.0f,mcolext.y*2.5f,0),&mtrans);
			break;
		}
		case SSG_GS_WIN: {
			D3DXVec3Transform(&cam_tpos_4,&D3DXVECTOR3(mcolext.x*5.0f,mcolext.y*5.5f,0),&mtrans);
			break;
		}
		case SSG_GS_LOSE: {
			D3DXVec3Transform(&cam_tpos_4,&D3DXVECTOR3(-mcolext.x*5.0f,mcolext.y*5.5f,0),&mtrans);
			break;
		}
	}

	D3DXVECTOR3 ret;
	ret.x = cam_tpos_4.x;
	ret.y = cam_tpos_4.y;
	ret.z = cam_tpos_4.z;
	return ret;
}

State_SpaceGame_Entity_Enemy::State_SpaceGame_Entity_Enemy(int id,State_SpaceGame *parent) : State_SpaceGame_Entity_Base(id,parent) {
	mtype = SSG_ENTTYPE_ENEMY;

	{
		D3DXMATRIX basetrans;
		D3DXMatrixRotationQuaternion(&basetrans,&quaterionRotateTo(XUNIT3,SSG_DIR_FORWARD));

		{
			D3DXMATRIX mat_scale;
			D3DXMatrixScaling(&mat_scale,3.5f,3.5f,3.5f);
			basetrans = basetrans * mat_scale;
		}

		HDXMesh *mesh = HDXMeshLoadFromX("data/mesh/spacechase/enemy_ship.x","data/texture/spacechase/enemy_ship/",true);

		setMesh(mesh,basetrans);

		//original mesh has no ambient or specular
		if(mmesh) {
			for(unsigned int i = 0; i < mmesh->mmaterials.size(); i++) {
				mmesh->mmaterials[i].ambient = HDXCOL_WHITE*0.5f;
				mmesh->mmaterials[i].specular = HDXCOL_WHITE*0.5f;
				mmesh->mmaterials[i].power = 10;
			}
		}
	}

	mpos += SSG_DIR_FORWARD * 125.0f;
	mvel += SSG_DIR_FORWARD * SSG_ENT_ENEMY_SPEED_START;

	mdroptimer = 2.0f; // don't drop anything for a few seconds
}

State_SpaceGame_Entity_Enemy::~State_SpaceGame_Entity_Enemy() {
}

void State_SpaceGame_Entity_Enemy::update(float dt) {
	State_SpaceGame_Entity_Base::update(dt);

	mdroptimer -= dt;
	if(mdroptimer <= 0.0f) {
		mdroptimer = FRANDRANGE(SSG_ENT_ENEMY_DROPRATE_MIN,SSG_ENT_ENEMY_DROPRATE_MAX);

		State_SpaceGame_Entity_Base *newent = 0;

		{
			float choice = FRAND();

			if(choice <= 0.7f) {
				int newid = mparent->createEntity<State_SpaceGame_Entity_Asteroid>();
				newent = mparent->getEntity(newid);
			} else if(choice <= 0.9f) {
				int newid = mparent->createEntity<State_SpaceGame_Entity_Pickup_SpeedBoost>();
				newent = mparent->getEntity(newid);

				newent->mvel += FRANDRANGE(-0.75f,-3.0f) * SSG_DIR_FORWARD;
			} else {
			}
		}

		if(newent) {
			{
				D3DXVECTOR3 scale = matrixGetScale(mtrans);

				newent->mpos = mpos +
					-(SSG_DIR_FORWARD*(mcolext.x*scale.x+15.0f)) +
					(SSG_DIR_RIGHT*FRANDRANGE(-1.0f,1.0f)*(mcolext.z*scale.z*3.0f)) +
					(SSG_DIR_UP*FRANDRANGE(-1.0f,1.0f)*(mcolext.y*scale.y*3.5f));
			}

			newent->mvel += FRANDRANGE(-0.5f,0.5f) * SSG_DIR_FORWARD;
			newent->mvel += FRANDRANGE(-0.2f,0.2f) * SSG_DIR_RIGHT;
			newent->mvel += FRANDRANGE(-0.2f,0.2f) * SSG_DIR_UP;

			D3DXQuaternionRotationYawPitchRoll(&newent->mori,
											   FRANDRANGE(-(float)M_PI,(float)M_PI),
											   FRANDRANGE(-(float)M_PI,(float)M_PI),
											   FRANDRANGE(-(float)M_PI,(float)M_PI));

			for(int i = 0; i < 3; i++) {
				newent->mavel[i] = FRANDRANGE(-0.3f,0.3f);
			}
		}
	}
}

void State_SpaceGame_Entity_Enemy::draw(HDXShader_PhongLighting *shader) {
	State_SpaceGame_Entity_Base::draw(shader);
}

State_SpaceGame_Entity_Debris::State_SpaceGame_Entity_Debris(int id,State_SpaceGame *parent) : State_SpaceGame_Entity_Base(id,parent) {
	mtype = SSG_ENTTYPE_DEBRIS;
}

State_SpaceGame_Entity_Debris::~State_SpaceGame_Entity_Debris() {
}

void State_SpaceGame_Entity_Debris::update(float dt) {
	State_SpaceGame_Entity_Base::update(dt);
	{
		float pos_dir_my,pos_dir_player;
		pos_dir_my = D3DXVec3Dot(&mpos,&SSG_DIR_FORWARD);
		pos_dir_player = D3DXVec3Dot(&mparent->getPlayer()->mpos,&SSG_DIR_FORWARD);

		if(pos_dir_my+500 < pos_dir_player) {
			markForDeletion();
		}
	}
}

void State_SpaceGame_Entity_Debris::draw(HDXShader_PhongLighting *shader) {
	State_SpaceGame_Entity_Base::draw(shader);
}

State_SpaceGame_Entity_Asteroid::State_SpaceGame_Entity_Asteroid(int id,State_SpaceGame *parent) : State_SpaceGame_Entity_Debris(id,parent) {
	mtype = SSG_ENTTYPE_ASTEROID;

	{
		D3DXMATRIX basetrans;
		D3DXMatrixRotationQuaternion(&basetrans,&quaterionRotateTo(XUNIT3,SSG_DIR_FORWARD));

		{
			D3DXMATRIX mat_scale;
			const float scale = 0.4f * FRANDRANGE(0.5f,1.25f);
			D3DXMatrixScaling(&mat_scale,scale,scale,scale);
			basetrans = basetrans * mat_scale;
		}

		{
			auto meshes = mparent->getPreloadedMeshes();
			setMesh(HDXMeshLoadFromX((*meshes)[rand()%meshes->size()]->getName(),"data/texture/spacechase/asteroid/",true),basetrans);

			auto textures = mparent->getPreloadedTextures();
			mtexture = (*textures)[rand()%textures->size()];
		}

		//original mesh has no ambient or specular
		if(mmesh) {
			for(unsigned int i = 0; i < mmesh->mmaterials.size(); i++) {
				mmesh->mmaterials[i].ambient = HDXCOL_WHITE*0.5f;
				mmesh->mmaterials[i].specular = HDXCOL_WHITE*0.5f;
				mmesh->mmaterials[i].power = 10;
			}
		}
	}
}

State_SpaceGame_Entity_Asteroid::~State_SpaceGame_Entity_Asteroid() {
}

void State_SpaceGame_Entity_Asteroid::update(float dt) {
	State_SpaceGame_Entity_Debris::update(dt);
	{
		float pos_dir_my,pos_dir_player;
		pos_dir_my = D3DXVec3Dot(&mpos,&SSG_DIR_FORWARD);
		pos_dir_player = D3DXVec3Dot(&mparent->getPlayer()->mpos,&SSG_DIR_FORWARD);

		if(pos_dir_my+50 < pos_dir_player) {
			markForDeletion();
		}
	}
}

void State_SpaceGame_Entity_Asteroid::draw(HDXShader_PhongLighting *shader) {
	IDirect3DDevice9 *device = shader->getD3DDevice();

	if(device && mmesh) {
		shader->pushMatrixWorld();

		shader->multMatrixWorld(mtrans);

		for(unsigned int i = 0; i < mmesh->mmaterials.size(); i++) {
			shader->setMaterial(mmesh->mmaterials[i]);
			shader->setTexture(mtexture);
			mmesh->md3dmesh->DrawSubset(i);
		}

		shader->popMatrixWorld();
	}
}

State_SpaceGame_Entity_Pickup_SpeedBoost::State_SpaceGame_Entity_Pickup_SpeedBoost(int id,State_SpaceGame *parent) : State_SpaceGame_Entity_Debris(id,parent) {
	mtype = SSG_ENTTYPE_PICKUP_SPEEDBOOST;

	{
		D3DXMATRIX basetrans;
		D3DXMatrixRotationQuaternion(&basetrans,&quaterionRotateTo(XUNIT3,SSG_DIR_FORWARD));

		HDXMesh *mesh = HDXMeshCreateSphere(2.5f,HDXMAT_WHITE);
		setMesh(mesh,basetrans);

		if(mmesh) {
			mmesh->mmaterials[0].emisive = HDXCOL_BLUE;
		}
	}

	mlight_point = mparent->getShader()->createLight();
	mlight_point->setLightPoint(HDXCOL_BLACK,
								HDXCOL_BLUE*1.75f,
								HDXCOL_BLUE*0.75f,
								D3DXVECTOR3(0,0,0),
								D3DXVECTOR3(0,0,0.002f));
	mlight_point->setEnabled(true);
}

State_SpaceGame_Entity_Pickup_SpeedBoost::~State_SpaceGame_Entity_Pickup_SpeedBoost() {
	mparent->getShader()->destroyLight(mlight_point->getID());
}

void State_SpaceGame_Entity_Pickup_SpeedBoost::update(float dt) {
	State_SpaceGame_Entity_Debris::update(dt);

	mlight_point->setPosition(mpos);
}

void State_SpaceGame::stateEnter() {
	HDX_MAIN->setWindowClearColor(0xff000000);

	HDXCamera *cam = HDX_MAIN->getCameraCurrent();

	cam->setPosition(D3DXVECTOR3(-10,10,0));
	cam->setLookAt(D3DXVECTOR3(0,0,0));

	mhdxfx = new HDXShader_PhongLighting(HDX_MAIN->getD3DDevice(),"data/shader/lighting.fx","phong_tech");

	//preload debris models and textures to prevent stuttering
	{
		for(int i = 0; i < 3; i++) {
			std::stringstream ss;
			ss << "data/mesh/spacechase/asteroid_" << i << ".x";
			mpreload_mesh.push_back(HDXMeshLoadFromX(ss.str(),"data/texture/spacechase/asteroid/",true));
		}

		for(int i = 0; i < 6; i++) {
			std::stringstream ss;
			ss << "data/texture/spacechase/asteroid/asteroid_" << i << ".jpg";
			mpreload_texture.push_back(HDXTextureCreateFromFile(ss.str(),true));
		}
	}

	//the music file is NOT managed, as it needs to persist for cross fading
	HDX_SOUNDMAN->loadSoundStream("data/sound/spacechase/music_background.mp3",FMOD_DEFAULT);
	HDX_SOUNDMAN->loadSoundStream("data/sound/spacechase/music_gamewin.mp3",FMOD_DEFAULT);
	HDX_SOUNDMAN->loadSoundStream("data/sound/spacechase/music_gamelose.mp3",FMOD_DEFAULT);

	mlight_directional = mhdxfx->createLight();
	mlight_directional->setLightDirectional(HDXCOL_WHITE*0.2f,
											HDXCOL_WHITE*0.7f,
											HDXCOL_RED*1.5f,
											D3DXVECTOR3(-1.0f,-3.5f,-1.0f));
	mlight_directional->setEnabled(true);

	mgamestate = SSG_GS_NONE;

	mcbid_onkeypress = HDX_INPUT->registerCallbackOnKeyPress([this](WPARAM key) {
		if(mgamestate != SSG_GS_PLAYING) {
			if(key == 'R') {
				MSGMAN->enqueueMessage(0,-1,"state_SpaceGame:gameReset");
			}
		}
	});

	mmsgregid_entitydelete = addMessageSubscription(-1,"state_SpaceGame:deleteEntity",MSGCBFROMLAM(this,int id) {
		destroyEntity(id);
	});

	mmsgregid_gamereset = addMessageSubscription(-1,"state_SpaceGame:gameReset",MSGCBFROMLAM(this) {
		mgamestate = SSG_GS_PLAYING;
		destroyAllEntities();

		mentid_player = createEntity<State_SpaceGame_Entity_Player>();
		mentid_enemy = createEntity<State_SpaceGame_Entity_Enemy>();

		HDX_MUSICMAN->setTrackCrossfade(HDX_SOUNDMAN->getSoundStream("data/sound/spacechase/music_background.mp3"),3.0f);
	});

	mmsgregid_ongamewin = addMessageSubscription(-1,"state_SpaceGame:onGameWin",MSGCBFROMLAM(this) {
		mgamestate = SSG_GS_WIN;
		HDX_MUSICMAN->setTrackCrossfade(HDX_SOUNDMAN->getSoundStream("data/sound/spacechase/music_gamewin.mp3"),3.0f);
	});

	mmsgregid_ongamelose = addMessageSubscription(-1,"state_SpaceGame:onGameLose",MSGCBFROMLAM(this) {
		mgamestate = SSG_GS_LOSE;
		HDX_MUSICMAN->setTrackCrossfade(HDX_SOUNDMAN->getSoundStream("data/sound/spacechase/music_gamelose.mp3"),3.0f);
		//MSGMAN->enqueueMessage(5000,-1,"state_SpaceGame:gameReset");
	});

	MSGMAN->enqueueMessage(0,-1,"state_SpaceGame:gameReset");
}

void State_SpaceGame::stateExecute(float dt) {
	std::vector<State_SpaceGame_Entity_Base*> coltest_list;
	coltest_list.reserve(mentities.size());

	mentities.doIterate([&](State_SpaceGame_Entity_Base *ent)->bool {
		if(ent->mactive) {
			coltest_list.push_back(ent);
			ent->update(dt);
		}
		return false;
	});

	for(auto it0 = coltest_list.begin(); it0 != coltest_list.end(); it0++) {
		for(auto it1 = it0+1; it1 != coltest_list.end(); it1++) {
			if(HDX9IntersectionTests::obb_obb(&(*it0)->mcolext,&(*it0)->mtrans,&(*it1)->mcolext,&(*it1)->mtrans)) {
				MSGMAN->enqueueMessage(0,(*it0)->mid,"state_SpaceGame:onCollision",(*it1)->mid);
				MSGMAN->enqueueMessage(0,(*it1)->mid,"state_SpaceGame:onCollision",(*it0)->mid);
			}
		}
	}
}

void State_SpaceGame::stateDraw() {
	IDirect3DDevice9 *device = mhdxfx->getD3DDevice();

	if(device) {
		mhdxfx->doDraw([&](HDXShader_Base *hdxfx)->void {
			hdxfx->setWVPFromDevice();

			mentities.doIterate([&](State_SpaceGame_Entity_Base *ent)->bool {
				if(ent->mactive) {
					ent->draw(mhdxfx);
				}
				return false;
			});
		});

		if(mgamestate != SSG_GS_PLAYING) {
			HDXFontBase *font = HDX_FONTMAN->getFont("DX:Arial_15");
			if(font) {
				RECT trect;
				{
					int sx,sy;
					HDX_MAIN->getWindowSize(&sx,&sy);
					rectSetPosSize(&trect,sx/2,sy/4,sx,sy/2);
				}

				if(mgamestate == SSG_GS_WIN) {
					font->drawText("     WINNER\n\n"
								   "Press 'R' to restart.",trect,DT_CENTER|DT_VCENTER,0xffffffff);
				}

				if(mgamestate == SSG_GS_LOSE) {
					font->drawText("     LOSER\n\n"
								   "Press 'R' to restart.",trect,DT_CENTER|DT_VCENTER,0xffff5555);
				}
			}
		}
	}
}

void State_SpaceGame::stateExit() {
	destroyAllEntities();

	for(unsigned i = 0; i < mpreload_mesh.size(); i++) {
		mpreload_mesh[i]->release();
	}

	for(unsigned i = 0; i < mpreload_texture.size(); i++) {
		mpreload_texture[i]->release();
	}

	removeMessageSubscription(mmsgregid_entitydelete);
	removeMessageSubscription(mmsgregid_gamereset);
	removeMessageSubscription(mmsgregid_ongamewin);
	removeMessageSubscription(mmsgregid_ongamelose);

	HDX_INPUT->unregisterCallbackOnKeyPress(mcbid_onkeypress);

	delete mhdxfx;
}

void State_SpaceGame::stateOnLostDevice() {
}

void State_SpaceGame::stateOnResetDevice() {
}

HDXShader_PhongLighting* State_SpaceGame::getShader() {
	return mhdxfx;
}

std::vector<HDXMesh*>* State_SpaceGame::getPreloadedMeshes() {
	return &mpreload_mesh;
}

std::vector<HDXTexture*>* State_SpaceGame::getPreloadedTextures() {
	return &mpreload_texture;
}

State_SpaceGame_Entity_Player* State_SpaceGame::getPlayer() {
	return (State_SpaceGame_Entity_Player*)getEntity(mentid_player);
}

State_SpaceGame_Entity_Enemy* State_SpaceGame::getEnemy() {
	return (State_SpaceGame_Entity_Enemy*)getEntity(mentid_enemy);
}

SSG_GAMESTATE State_SpaceGame::getGameState() {
	return mgamestate;
}

State_SpaceGame_Entity_Base* State_SpaceGame::getEntity(int id) {
	return mentities.getItem(id);
}

void State_SpaceGame::destroyEntity(int id) {
	State_SpaceGame_Entity_Base *ent = mentities.getItem(id);
	if(ent) {
		mentities.removeItem(id);
		delete ent;
	}
}

void State_SpaceGame::destroyAllEntities() {
	mentities.doIterate([](State_SpaceGame_Entity_Base *ent)->bool {
		delete ent;
		return false;
	});
	mentities.clear();
}