#pragma once

#include "hdx9ui_elementeffect_base.h"

class HDXTexture;

class HDXUIElementEffect_TextureDraw : public HDXUIElementEffect_Base {
public:
	HDXUIElementEffect_TextureDraw(const std::string &name,
								   HDXUIElementBase *parent,
								   const std::string &tex);
	virtual ~HDXUIElementEffect_TextureDraw();

	std::string getTexture() const;
	void setTexture(const std::string &tex);

	D3DXCOLOR getBlendColor() const;
	void setBlendColor(D3DXCOLOR blendcolor);

	void getSourceRect(RECT *rect) const;
	void setSourceRect(const RECT *rect);
	int getSourceWidth() const;
	int getSourceHeight() const;
	void setSourceRectToWholeTexture();

	D3DXVECTOR3 getSourceCenter() const;
	void getSourceCenter(D3DXVECTOR3 *center) const;
	void setSourceCenter(const D3DXVECTOR3 *center);
	void setSourceCenterToCenterSourceRect();

	virtual float getZPos() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransiton);

	virtual void update(float dt);
	virtual void render();

private:
	HDXTexture *mtexture;
	RECT msource_rect;
	D3DXVECTOR3 msource_center;
	D3DXCOLOR mblendcolor;
};
