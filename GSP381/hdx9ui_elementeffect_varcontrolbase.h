#pragma once

#pragma warning(disable:4250)

#include "hdx9ui_elementeffect_transition_base.h"

template<typename T>
class HDXUIElementEffect_VarControlBase : virtual public HDXUIElementEffect_Base {
public:
	HDXUIElementEffect_VarControlBase(const std::string &name,
									  HDXUIElementBase *parent,
									  std::function<T(void)> func_varget,
									  std::function<void(const T &var)> func_varset);
	virtual ~HDXUIElementEffect_VarControlBase();

	T getEffect() const;

	T getFinal() const;
	void setFinal(const T &var);

	void setTargetFuncGet(std::function<T(void)> func_varget);
	void setTargetFuncSet(std::function<void(const T &var)> func_varset);

	virtual void update(float dt);

protected:
	virtual T _getEffect() const = 0;
	virtual void _onDelta(const T &delta) = 0;

private:
	std::function<T(void)> mfunc_varget;
	std::function<void(const T &var)> mfunc_varset;
	T mvar_final,mvar_last,mvar_effect;
	bool mvar_aquired,mvar_peeked;

	void _checkDelta();
};

template<typename T>
HDXUIElementEffect_VarControlBase<T>::HDXUIElementEffect_VarControlBase(const std::string &name,
																		HDXUIElementBase *parent,
																		std::function<T(void)> func_varget,
																		std::function<void(const T &var)> func_varset) : HDXUIElementEffect_Base(name,parent) {
	mvar_aquired = false;
	mvar_peeked = false;
	setTargetFuncGet(func_varget);
	setTargetFuncSet(func_varset);
}

template<typename T>
HDXUIElementEffect_VarControlBase<T>::~HDXUIElementEffect_VarControlBase() {
}

template<typename T>
T HDXUIElementEffect_VarControlBase<T>::getEffect() const {
	return mvar_effect;
}

template<typename T>
T HDXUIElementEffect_VarControlBase<T>::getFinal() const {
	return mvar_final;
}

template<typename T>
void HDXUIElementEffect_VarControlBase<T>::setFinal(const T &var) {
	mvar_final = mvar_last = var;
	mvar_aquired = true;
}

template<typename T>
void HDXUIElementEffect_VarControlBase<T>::setTargetFuncGet(std::function<T(void)> func_varget) {
	mfunc_varget = func_varget;

	if(func_varget!=nullptr && !mvar_aquired && !mvar_peeked) {
		mvar_peeked = true;
		mvar_final = mfunc_varget();
	}
}

template<typename T>
void HDXUIElementEffect_VarControlBase<T>::setTargetFuncSet(std::function<void(const T &var)> func_varset) {
	mfunc_varset = func_varset;
}

template<typename T>
void HDXUIElementEffect_VarControlBase<T>::update(float dt) {
	HDXUIElementEffect_Base::update(dt);
	_checkDelta();
	T effect = _getEffect();
	mvar_last = mvar_final + effect;
	mfunc_varset(mvar_last);
}

template<typename T>
void HDXUIElementEffect_VarControlBase<T>::_checkDelta() {
	T var_cur = mfunc_varget();

	auto lam_check = [&](const T &prev) {
		T delta = var_cur - prev;
		mvar_final += delta;
		_onDelta(delta);
	};

	if(mvar_aquired) {
		lam_check(mvar_last);
	} else {
		mvar_aquired = true;
		if(mvar_peeked) {
			lam_check(mvar_final);
		} else {
			mvar_final = var_cur;
		}
	}
}
