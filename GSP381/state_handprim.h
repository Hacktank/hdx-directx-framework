#pragma once

#include "basedx9state.h"
#include "basemanager.h"

class HDXTexture;

class State_HandPrim : public BaseDX9State {
public:
	State_HandPrim() {}
	virtual ~State_HandPrim() {}

	virtual void stateEnter();
	virtual void stateExecute(float dt);
	virtual void stateDraw();
	virtual void stateExit();
	virtual void stateOnLostDevice();
	virtual void stateOnResetDevice();

private:
	IDirect3DVertexBuffer9 *mvb;
	IDirect3DIndexBuffer9 *mib;
	HDXTexture *mtexture;

	int mcbid_input_mousemove;
	int mcbid_input_mousebuttonpress;
	int mcbid_input_mousebuttonrelease;

	bool mrotating;
	D3DXVECTOR3 mrot;
};
