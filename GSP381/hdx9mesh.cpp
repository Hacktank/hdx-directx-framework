#include "hdx9mesh.h"
#include "hdx9texture.h"

#include "hdx9.h"
#include "hdx9vertex.h"

std::unordered_map<std::string,HDXMesh*> HDXMesh::__managed_items;

HDXMesh::HDXMesh() {
	mmanaged = false;
	musers = 0;
	md3dmesh = 0;
}

HDXMesh::~HDXMesh() {
	if(md3dmesh) {
		md3dmesh->Release();
	}

	for(auto it = mtextures.begin(); it != mtextures.end(); it++) {
		if((*it)) (*it)->release();
	}

	if(mmanaged) {
		__managed_items.erase(mname);
	}
}

std::string HDXMesh::getName() const {
	return mname;
}

int HDXMesh::release() {
	int ret = --musers;
	if(musers==0) {
		delete this;
	}
	return ret;
}

HDXMesh* HDXMesh::__create(const std::string &name,bool managed) {
	HDXMesh *ret = 0;
	if(managed) {
		if(__managed_items.count(name) > 0) {
			ret = __managed_items[name];
		}
	}

	if(ret == 0) {
		ret = new HDXMesh();
		ret->mname = name;
		ret->mmanaged = managed;

		if(managed) {
			__managed_items[name] = ret;
		}
	}

	ret->musers++;

	return ret;
}

HDXMesh* HDXMeshLoadFromX(const std::string &fname,const std::string &texturepath,bool managed) {
	bool isnew = HDXMesh::__managed_items.count(fname) == 0;

	HDXMesh *ret = HDXMesh::__create(fname,managed);

	if(isnew) {
		ID3DXMesh *meshsys = 0;
		ID3DXBuffer *adjbuffer = 0;
		ID3DXBuffer *mtrlbuffer = 0;
		DWORD nummtrls = 0;

		D3DXLoadMeshFromX(fname.c_str(),
						  D3DXMESH_SYSTEMMEM,
						  HDX_MAIN->getD3DDevice(),
						  &adjbuffer,
						  &mtrlbuffer,
						  0,
						  &nummtrls,
						  &meshsys);

		bool hasnormals = false;
		bool hascolor = false;

		{
			D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
			meshsys->GetDeclaration(elems);

			for(int i = 0; i < MAX_FVF_DECL_SIZE; i++) {
				if(elems[i].Stream==0xff) break; //reached end

				if((elems[i].Type == D3DDECLTYPE_FLOAT3) &&
				   (elems[i].Usage == D3DDECLUSAGE_NORMAL) &&
				   (elems[i].UsageIndex == 0)) {
					hasnormals = true;
				}

				if((elems[i].Type == D3DDECLTYPE_FLOAT4) &&
				   (elems[i].Usage == D3DDECLUSAGE_COLOR) &&
				   (elems[i].UsageIndex == 0)) {
					hascolor = true;
				}
			}
		}

		{
			D3DVERTEXELEMENT9 elements[MAX_FVF_DECL_SIZE-1];
			UINT numelements = 0;
			HDXVERTPCTN::decl->GetDeclaration(elements,&numelements);

			ID3DXMesh *tmp = 0;
			HRESULT hr = meshsys->CloneMesh(D3DXMESH_SYSTEMMEM,
							   elements,
							   HDX_MAIN->getD3DDevice(),
							   &tmp);

			meshsys->Release();
			meshsys = tmp;
		}

		if(!hasnormals) {
			D3DXComputeNormals(meshsys,0);
		}

		meshsys->Optimize(D3DXMESH_MANAGED |
						  D3DXMESHOPT_COMPACT |
						  D3DXMESHOPT_ATTRSORT |
						  D3DXMESHOPT_VERTEXCACHE,
						  (DWORD*)adjbuffer->GetBufferPointer(),
						  0,
						  0,
						  0,
						  &(ret->md3dmesh));
		meshsys->Release();
		adjbuffer->Release();

		if(!hascolor) {
			//original decl did not have color, so the points will all have the color 0x0 (black)
			IDirect3DVertexBuffer9 *vb;
			ret->md3dmesh->GetVertexBuffer(&vb);

			HDXVERTPCTN *verts;
			vb->Lock(0,ret->md3dmesh->GetNumVertices()*sizeof(HDXVERTPCTN),(void**)&verts,0);

			for(unsigned int i = 0; i < ret->md3dmesh->GetNumVertices(); i++) {
				verts[i].c = 0xffffffff;
			}

			vb->Unlock();
		}

		if(mtrlbuffer!=0 && nummtrls>0) {
			D3DXMATERIAL *d3dxmtrls = (D3DXMATERIAL*)mtrlbuffer->GetBufferPointer();

			for(DWORD i = 0; i < nummtrls; i++) {
				HDXMaterial mat;
				mat.ambient = d3dxmtrls[i].MatD3D.Ambient;
				mat.diffuse = d3dxmtrls[i].MatD3D.Diffuse;
				mat.specular = d3dxmtrls[i].MatD3D.Specular;
				mat.emisive = d3dxmtrls[i].MatD3D.Emissive;
				mat.power = d3dxmtrls[i].MatD3D.Power;
				ret->mmaterials.push_back(mat);

				//load associated textures
				if(d3dxmtrls[i].pTextureFilename != 0) {
					ret->mtextures.push_back(HDXTextureCreateFromFile(texturepath + d3dxmtrls[i].pTextureFilename,true));
				} else {
					//to keep material and texture IDs connected to subset ids
					ret->mtextures.push_back(0);
				}
			}
		}
	}

	return ret;
}

HDXMesh* HDXMeshCreateBlank(const std::string &name,bool managed) {
	return HDXMesh::__create(name,managed);
}

HDXMesh* HDXMeshCreateSphere(float rad,const HDXMaterial &mat) {
	HDXMesh *ret = HDXMeshCreateBlank("",false);

	D3DXCreateSphere(HDX_MAIN->getD3DDevice(),
					 rad,
					 20,
					 20,
					 &ret->md3dmesh,
					 0);

	//convert to PCTN vertex format
	{
		D3DVERTEXELEMENT9 elements[MAX_FVF_DECL_SIZE-1];
		UINT numelements = 0;
		HDXVERTPCTN::decl->GetDeclaration(elements,&numelements);

		ID3DXMesh *tmp = 0;
		ret->md3dmesh->CloneMesh(D3DXMESH_MANAGED,
								 elements,
								 HDX_MAIN->getD3DDevice(),
								 &tmp);

		ret->md3dmesh->Release();
		ret->md3dmesh = tmp;
	}

	//set vert colors to white
	{
		IDirect3DVertexBuffer9 *vb;
		ret->md3dmesh->GetVertexBuffer(&vb);

		HDXVERTPCTN *verts;
		vb->Lock(0,ret->md3dmesh->GetNumVertices()*sizeof(HDXVERTPCTN),(void**)&verts,0);

		for(unsigned int i = 0; i < ret->md3dmesh->GetNumVertices(); i++) {
			verts[i].c = 0xffffffff;
		}

		vb->Unlock();
	}

	ret->mmaterials.push_back(mat);
	ret->mtextures.push_back(0);

	return ret;
}

HDXMesh* HDXMeshCreateBox(const D3DXVECTOR3 &ext,const HDXMaterial &mat) {
	HDXMesh *ret = HDXMeshCreateBlank("",false);

	D3DXCreateBox(HDX_MAIN->getD3DDevice(),
				  ext.x*2,
				  ext.y*2,
				  ext.z*2,
				  &ret->md3dmesh,
				  0);

	//convert to PCTN vertex format
	{
		D3DVERTEXELEMENT9 elements[MAX_FVF_DECL_SIZE-1];
		UINT numelements = 0;
		HDXVERTPCTN::decl->GetDeclaration(elements,&numelements);

		ID3DXMesh *tmp = 0;
		ret->md3dmesh->CloneMesh(D3DXMESH_MANAGED,
								 elements,
								 HDX_MAIN->getD3DDevice(),
								 &tmp);

		ret->md3dmesh->Release();
		ret->md3dmesh = tmp;
	}

	//set vert colors to white
	{
		IDirect3DVertexBuffer9 *vb;
		ret->md3dmesh->GetVertexBuffer(&vb);

		HDXVERTPCTN *verts;
		vb->Lock(0,ret->md3dmesh->GetNumVertices()*sizeof(HDXVERTPCTN),(void**)&verts,0);

		for(unsigned int i = 0; i < ret->md3dmesh->GetNumVertices(); i++) {
			verts[i].c = 0xffffffff;
		}

		vb->Unlock();
	}

	ret->mmaterials.push_back(mat);
	ret->mtextures.push_back(0);

	return ret;
}