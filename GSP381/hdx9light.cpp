
#include "hdx9light.h"


HDXLight::HDXLight(int id) {
	mtype = -1;
	mid = id;
	menabled = false;
}

HDXLight::~HDXLight() {

}

int HDXLight::getType() const {
	return mtype;
}

int HDXLight::getID() const {
	return mid;
}

bool HDXLight::isEnabled() const {
	return menabled;
}

void HDXLight::setEnabled(bool enabled) {
	menabled = enabled;
}

D3DXCOLOR HDXLight::getColorAmbient() const {
	return mcol_ambient;
}

void HDXLight::setColorAmbient(const D3DXCOLOR &col) {
	mcol_ambient = col;
}

D3DXCOLOR HDXLight::getColorDiffuse() const {
	return mcol_diffuse;
}

void HDXLight::setColorDiffuse(const D3DXCOLOR &col) {
	mcol_diffuse = col;
}

D3DXCOLOR HDXLight::getColorSpecular() const {
	return mcol_specular;
}

void HDXLight::setColorSpecular(const D3DXCOLOR &col) {
	mcol_specular = col;
}

D3DXVECTOR3 HDXLight::getPosition() const {
	return mpos;
}

void HDXLight::setPosition(const D3DXVECTOR3 &v) {
	mpos = v;
}

D3DXVECTOR3 HDXLight::getDirection() const {
	return mdirection;
}

void HDXLight::setDirection(const D3DXVECTOR3 &v) {
	mdirection = v;
	D3DXVec3Normalize(&mdirection,&mdirection);
}

D3DXVECTOR3 HDXLight::getAttenuation() const {
	return mattenuation;
}

void HDXLight::setAttenuation(const D3DXVECTOR3 &v) {
	mattenuation = v;
}

float HDXLight::getFalloff() const {
	return mfalloff;
}

void HDXLight::setFalloff(float falloff) {
	mfalloff = falloff;
}

float HDXLight::getTheta() const {
	return mtheta;
}

void HDXLight::setTheta(float theta) {
	mtheta = theta;
}

float HDXLight::getPhi() const {
	return mphi;
}

void HDXLight::setPhi(float phi) {
	mphi = phi;
}

void HDXLight::setLightDirectional(const D3DXCOLOR &ambient,
								   const D3DXCOLOR &diffuse,
								   const D3DXCOLOR &specular,
								   const D3DXVECTOR3 &direction) {
	mtype = HDXLT_DIRECTIONAL;

	setColorAmbient(ambient);

	setColorAmbient(ambient);
	setColorDiffuse(diffuse);
	setColorSpecular(specular);

	setDirection(direction);
}

void HDXLight::setLightPoint(const D3DXCOLOR &ambient,
							 const D3DXCOLOR &diffuse,
							 const D3DXCOLOR &specular,
							 const D3DXVECTOR3 &pos,
							 const D3DXVECTOR3 &attenuation) {
	mtype = HDXLT_POINT;

	setColorAmbient(ambient);
	setColorDiffuse(diffuse);
	setColorSpecular(specular);

	setPosition(pos);
	setAttenuation(attenuation);
}

void HDXLight::setLightSpot(const D3DXCOLOR &ambient,
							const D3DXCOLOR &diffuse,
							const D3DXCOLOR &specular,
							const D3DXVECTOR3 &pos,
							const D3DXVECTOR3 &attenuation,
							const D3DXVECTOR3 &direction,
							float falloff,
							float theta,
							float phi) {
	mtype = HDXLT_SPOT;

	setColorAmbient(ambient);
	setColorDiffuse(diffuse);
	setColorSpecular(specular);

	setPosition(pos);
	setAttenuation(attenuation);

	setDirection(direction);
	setFalloff(falloff);
	setTheta(theta);
	setPhi(phi);
}
