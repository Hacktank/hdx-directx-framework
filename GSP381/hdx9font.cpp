#include "hdx9font.h"
#include "hdx9.h"
#include "hdx9ui.h"

RECT HDX_RECT_POINT(int x,int y) {
	RECT ret;
	SetRect(&ret,x,y,x,y);
	return ret;
}

HDXFontBase::HDXFontBase(const std::string &name) {
	mname = name;
}

HDXFontBase::~HDXFontBase() {
}

std::string HDXFontBase::getName() {
	return mname;
}

void HDXFontBase::calculateFormattedTextDrawRect(const char *str,RECT *rect,DWORD format,int x,int y,int w,int h) {
	RECT text_rect;
	calculateTextRect(str,&text_rect,format);

	rectSetPosSize(rect,
				   x,
				   y,
				   (std::max)(w,(int)RECTWIDTH(text_rect)),
				   (std::max)(h,(int)RECTHEIGHT(text_rect)));

	if((format&DT_CENTER) && (RECTHEIGHT(text_rect)<h)) {
		rect->top += (h-RECTHEIGHT(text_rect))/2;
	}
}

HDXFontDX2D::HDXFontDX2D(const std::string &name) : HDXFontBase(name) {
	md3dfont = 0;
}

HDXFontDX2D::~HDXFontDX2D() {
	if(md3dfont) md3dfont->Release();
}

void HDXFontDX2D::drawText(const char *str,const RECT &dest,DWORD format,D3DCOLOR color,ID3DXSprite *sprite) {
	md3dfont->DrawTextA(sprite,str,-1,(RECT*)&dest,format,color);
}

void HDXFontDX2D::calculateTextRect(const char *str,RECT *rect,DWORD format) {
	SetRect(rect,0,0,0,0);
	md3dfont->DrawTextA(0,str,-1,rect,format|DT_CALCRECT,0x0);
}

bool HDXFontDX2D::_build(const char *facename,int pointsize,int weight,bool italic,DWORD quality) {
	HDC hdc = GetDC(0);
	int pointheight = (MulDiv(pointsize,GetDeviceCaps(hdc,LOGPIXELSY),72));
	ReleaseDC(0,hdc);

	HRESULT hr = D3DXCreateFont(HDX_MAIN->getD3DDevice(),
								pointheight,
								0,
								weight,
								0,
								italic,
								DEFAULT_CHARSET,
								OUT_DEFAULT_PRECIS,
								quality,
								DEFAULT_PITCH | FF_DONTCARE,
								facename,
								&md3dfont);
	return md3dfont != 0;
}

void HDXFontDX2D::_onLostDevice() {
	if(md3dfont) md3dfont->OnLostDevice();
}

void HDXFontDX2D::_onResetDevice() {
	if(md3dfont) md3dfont->OnResetDevice();
}

/*
HDXFontDX3D::HDXFontDX3D(const std::string &name) : HDXFontBase(name) {
mmesh = 0;
}

HDXFontDX3D::~HDXFontDX3D() {
if(mmesh) mmesh->Release();
}

void HDXFontDX3D::drawText(const char *str,const RECT &dest,DWORD format,D3DCOLOR color) {
md3dfont->DrawTextA(0,str,-1,(RECT*)&dest,format,color);
}

void HDXFontDX3D::calculateTextRect(const char *str,RECT *rect,DWORD format) {
SetRect(rect,0,0,0,0);
md3dfont->DrawTextA(0,str,-1,rect,format|DT_CALCRECT,0x0);
}

bool HDXFontDX3D::_build(const char *facename,int pointsize,int weight,bool italic,DWORD quality) {
HDC hdc = GetDC(0);
int pointheight = -(MulDiv(pointsize,GetDeviceCaps(hdc,LOGPIXELSY),72));

LOGFONT lf;
lf.lfHeight = pointheight;
lf.lfWeight = 0;
lf.lfWeight = weight;
lf.lfItalic = italic;
lf.lfUnderline = false;
lf.lfStrikeOut = false;
lf.lfCharSet = DEFAULT_CHARSET;
strcpy(lf.lfFaceName,facename);

HFONT hfont;
HFONT hfontold;
hfont = CreateFontIndirect(&lf);
hfontold = (HFONT)SelectObject(hdc,hfont);

D3DXCreateText(HDX_MAIN->getD3DDevice(),
hdc,);
//http://www.cs.vu.nl/~eliens/design/@archive/projects/jurgen/directx9.pdf
ReleaseDC(0,hdc);

HRESULT hr = D3DXCreateFont(HDX_MAIN->getD3DDevice(),
pointheight,
0,
weight,
0,
italic,
DEFAULT_CHARSET,
OUT_DEFAULT_PRECIS,
quality,
DEFAULT_PITCH | FF_DONTCARE,
facename,
&md3dfont);
return md3dfont != 0;
}

void HDXFontDX3D::_onLostDevice() {
//if(mmesh) mmesh->onlo();
}

void HDXFontDX3D::_onResetDevice() {
//if(mmesh) mmesh->OnResetDevice();
}
*/

HDXFontManager::HDXFontManager() {
}

HDXFontManager::~HDXFontManager() {
	for(auto it = mfonts.begin(); it != mfonts.end(); it++) {
		delete it->second;
	}
}

HDXFontBase* HDXFontManager::getFont(const std::string &name) {
	if(mfonts.count(name) > 0) {
		return mfonts[name];
	}
	return 0;
}

void HDXFontManager::deleteFont(const std::string &name) {
	HDXFontBase *curfont = getFont(name);
	if(curfont) {
		delete curfont;
		mfonts.erase(name);
	}
}

HDXFontDX2D* HDXFontManager::createFontDX(const std::string &name,const char *facename,int pointsize,int weight /*= FW_NORMAL*/,bool italic /*= false*/,DWORD quality /*= DEFAULT_QUALITY*/) {
	HDXFontBase *curfont = getFont(name);
	if(!curfont) { //only if the font doesn't already exist
		curfont = new HDXFontDX2D(name);
		((HDXFontDX2D*)curfont)->_build(facename,pointsize,weight,italic,quality);
		mfonts[name] = curfont;
	}
	return (HDXFontDX2D*)curfont;
}

void HDXFontManager::_onLostDevice() {
	for(auto it = mfonts.begin(); it != mfonts.end(); it++) {
		it->second->_onLostDevice();
	}
}

void HDXFontManager::_onResetDevice() {
	for(auto it = mfonts.begin(); it != mfonts.end(); it++) {
		it->second->_onResetDevice();
	}
}