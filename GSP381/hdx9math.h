
#pragma once

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include <d3dx9.h>
#include <directxmath.h>

#define HDX9MATH_IJ(i,j) (4*j+i)

#define XUNIT2 (D3DXVECTOR2(1,0))
#define XUNIT3 (D3DXVECTOR3(1,0,0))
#define YUNIT2 (D3DXVECTOR2(0,1))
#define YUNIT3 (D3DXVECTOR3(0,1,0))
#define ZUNIT3 (D3DXVECTOR3(0,0,1))
#define VEC2TOARG(v) (v).x,(v).y
#define VEC3TOARG(v) (v).x,(v).y,(v).z

#ifndef CLAMP
#define CLAMP(n,nummin,nummax) ((n)<(nummin) ? (nummin):((n)>(nummax) ?(nummax):(n)))
#endif

#define RANDRANGE(a,b) ((rand()%(int)((b)+1-(a)))+(int)(a))
#define FRANDRANGE(a,b) (((float)rand()/(float)RAND_MAX)*((a)-(b))+(b))

D3DXVECTOR3 matrixGetRow(const D3DXMATRIX &mat,int ind);
D3DXVECTOR3 matrixGetColumn(const D3DXMATRIX &mat,int ind);
void matrixSetRow(D3DXMATRIX *mat,int ind,const D3DXVECTOR3 &vec);
void matrixSetColumn(D3DXMATRIX *mat,int ind,const D3DXVECTOR3 &vec);
D3DXVECTOR3 matrixGetScale(const D3DXMATRIX &mat);
D3DXMATRIX matrixLookDirection(const D3DXVECTOR3 &eye,const D3DXVECTOR3 &dir);

D3DXVECTOR3 vector3TransformQuaternion(const D3DXVECTOR3 &v,const D3DXQUATERNION &q);

D3DXQUATERNION quaterionRotateTo(const D3DXVECTOR3 &q,const D3DXVECTOR3 &p);
