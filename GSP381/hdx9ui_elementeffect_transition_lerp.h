#pragma once

#pragma warning(disable:4250)

#include "hdx9ui_elementeffect_transition_base.h"
#include "hdx9ui_elementeffect_varcontrolbase.h"

template<typename T>
class HDXUIElementEffect_Transition_LERP : public HDXUIElementEffect_VarControlBase<T>,public HDXUIElementEffect_Transition_Base {
public:
	HDXUIElementEffect_Transition_LERP(const std::string &name,
									   HDXUIElementBase *parent,
									   std::function<T(void)> func_varget,
									   std::function<void(const T &var)> func_varset,
									   DWORD runflags,
									   float transtime,
									   const T &target);
	virtual ~HDXUIElementEffect_Transition_LERP();

	T getTarget() const;
	void setTarget(const T &var);

	void swapValues();

	virtual void transition_onStart(bool enter);
	virtual void transition_onFinish();
	virtual void transition_onUpdate(float dt);

	virtual void update(float dt);

protected:
	virtual T _getEffect() const;
	virtual void _onDelta(const T &delta);

private:
	std::function<T(void)> mfunc_varget;
	std::function<void(const T &var)> mfunc_varset;
	T mtarget;
};

template<typename T>
HDXUIElementEffect_Transition_LERP<T>::HDXUIElementEffect_Transition_LERP(const std::string &name,
																		  HDXUIElementBase *parent,
																		  std::function<T(void)> func_varget,
																		  std::function<void(const T &var)> func_varset,
																		  DWORD runflags,
																		  float transtime,
																		  const T &target) :
																		  HDXUIElementEffect_VarControlBase(name,parent,func_varget,func_varset),
																		  HDXUIElementEffect_Transition_Base(name,parent,runflags,transtime),
																		  HDXUIElementEffect_Base(name,parent) {
	mtarget = target;
}

template<typename T>
HDXUIElementEffect_Transition_LERP<T>::~HDXUIElementEffect_Transition_LERP() {
}

template<typename T>
T HDXUIElementEffect_Transition_LERP<T>::getTarget() const {
	return mtarget;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::setTarget(const T &var) {
	mtarget = var;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::swapValues() {
	T tmp = getFinal();
	setFinal(mtarget);
	mtarget = tmp;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::transition_onStart(bool enter) {
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::transition_onFinish() {
	// only swap values if this transition runs on both entrance and exit
	if(getRunFlags()&HDX_TRANS_ENTER && getRunFlags()&HDX_TRANS_EXIT) {
		swapValues();
	}
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::transition_onUpdate(float dt) {
	HDXUIElementEffect_VarControlBase<T>::update(dt);
}

template<typename T>
T HDXUIElementEffect_Transition_LERP<T>::_getEffect() const {
	float progress = getProgress();
	return (mtarget-getFinal())*progress;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::_onDelta(const T &delta) {
	mtarget += delta;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::update(float dt) {
	HDXUIElementEffect_Transition_Base::update(dt);
	//do NOT run HDXUIElementEffect_VarControlBase's update here, it is in transition_onUpdate()
}
