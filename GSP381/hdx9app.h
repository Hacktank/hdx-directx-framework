#pragma once

#include <windows.h>

#include "hdx9ui.h"

struct IDirect3DDevice9;
struct ID3DXSprite;
class BaseDX9State;

class HDX9App {
	friend int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int); //to allow WinMain to access _run()
public:
	HDX9App();
	virtual ~HDX9App();

	//************************************
	// Method:    initializeScene
	// Returns:   void
	// Notes:
	//************************************
	virtual void initializeScene();

	//************************************
	// Method:    deinitializeScene
	// Returns:   void
	// Notes:
	//************************************
	virtual void deinitializeScene();

	//************************************
	// Method:    onDeviceLost
	// Returns:   void
	// Notes:
	//************************************
	virtual void onLostDevice();

	//************************************
	// Method:    onResetDevice
	// Returns:   void
	// Notes:
	//************************************
	virtual void onResetDevice();

	//************************************
	// Method:    updateScene
	// Returns:   void
	// Notes:
	//************************************
	virtual void updateScene(float dt);

	//************************************
	// Method:    renderScene
	// Returns:   void
	// Notes:
	//************************************
	virtual void renderScene();

	virtual void procMessage(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam);

	BaseDX9State* getCurrentState();
	void setState(BaseDX9State *state);

private:
	BaseDX9State *mcurstate;
	HDXUIMenuMap *mmenu_map;
};
