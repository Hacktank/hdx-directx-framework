#include "hdx9ui_element_button.h"

#include "hdx9ui_elementeffect_texturedraw.h"
#include "hdx9ui_elementeffect_textdraw.h"

#include "hdx9.h"
#include "hdx9font.h"

HDXUIElement_Button::HDXUIElement_Button(const std::string &name,
										 HDXUIElementBase *parent,
										 const std::string &maintexture,
										 const std::string &hovertexture,
										 const std::string &textfont,
										 const std::string &text,
										 DWORD textflags,
										 D3DXCOLOR textcolor) : HDXUIElementBase(name,parent) {
	meffect_texture_main = createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																		maintexture);

	meffect_texture_hover = createEffect<HDXUIElementEffect_TextureDraw>("texture_hover",
																		 hovertexture);

	meffect_text = createEffect<HDXUIElementEffect_TextDraw>("text_main",
															 textfont,
															 text,
															 textflags,
															 textcolor);

	RECT texture_source_rect;
	meffect_texture_main->getSourceRect(&texture_source_rect);
	setActionSize((float)RECTWIDTH(texture_source_rect),
				  (float)RECTHEIGHT(texture_source_rect));

	RECT text_draw_rect;
	rectSetPosSize(&text_draw_rect,(int)getPosition().x,(int)getPosition().y,(int)getActionWidth(),(int)getActionHeight());
	rectShrink(&text_draw_rect,5,5);

	meffect_text->setFontDrawRect(&text_draw_rect);
}

HDXUIElement_Button::~HDXUIElement_Button() {
}

HDXUIElementEffect_TextureDraw* HDXUIElement_Button::getMainTextureEffect() const {
	return meffect_texture_main;
}

HDXUIElementEffect_TextureDraw* HDXUIElement_Button::getHoverTextureEffect() const {
	return meffect_texture_hover;
}

HDXUIElementEffect_TextDraw* HDXUIElement_Button::getTextEffect() const {
	return meffect_text;
}

void HDXUIElement_Button::onMenuTransitionEnter(bool dotransition) {
	HDXUIElementBase::onMenuTransitionEnter(dotransition);
}

void HDXUIElement_Button::onMenuTransitionExit(bool dotransition) {
	HDXUIElementBase::onMenuTransitionExit(dotransition);
}

void HDXUIElement_Button::update(float dt) {
	HDXUIElementBase::update(dt);
	meffect_texture_hover->setActive(isMouseOver());
}

void HDXUIElement_Button::render() {
	HDXUIElementBase::render();
}