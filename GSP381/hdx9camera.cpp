
#include "hdx9camera.h"
#include "hdx9math.h"


HDXCamera::HDXCamera() {
	mpos.x = mpos.y = mpos.z = 0.0f;

	mori.x = mori.y = mori.z = 0.0f;
	mori.w = 1.0f;

	mchanged = true;
}

HDXCamera::~HDXCamera() {

}

D3DXVECTOR3 HDXCamera::getPosition() const {
	return mpos;
}

void HDXCamera::setPosition(const D3DXVECTOR3 &pos) {
	mchanged = true;
	mpos = pos;
}

void HDXCamera::setPosition(float x,float y,float z) {
	setPosition(D3DXVECTOR3(x,y,z));
}

D3DXQUATERNION HDXCamera::getOrientation() const {
	return mori;
}

void HDXCamera::setOrientation(const D3DXQUATERNION &ori) {
	mchanged = true;
	mori = ori;
}

void HDXCamera::translate(const D3DXVECTOR3 &v) {
	mchanged = true;
	mpos += v;
}

void HDXCamera::rotateAxis(const D3DXVECTOR3 &axis,float a) {
	mchanged = true;
	D3DXQUATERNION rotquat;
	D3DXQuaternionRotationAxis(&rotquat,&axis,-a); //rotation must be negative
	mori = rotquat * mori;
}

void HDXCamera::rotateYaw(float a) {
	rotateAxis(YUNIT3,a);
}

void HDXCamera::rotatePitch(float a) {
	rotateAxis(XUNIT3,a);
}

void HDXCamera::rotateRoll(float a) {
	rotateAxis(ZUNIT3,a);
}

D3DXVECTOR3 HDXCamera::getForward() const {
	D3DXMATRIX mat = getViewMatrix();
	return matrixGetColumn(mat,2);
}

D3DXVECTOR3 HDXCamera::getRight() const {
	D3DXMATRIX mat = getViewMatrix();
	return matrixGetColumn(mat,0);
}

D3DXVECTOR3 HDXCamera::getUp() const {
	D3DXMATRIX mat = getViewMatrix();
	return matrixGetColumn(mat,1);
}

void HDXCamera::setLookAt(const D3DXVECTOR3 &point) {
	mchanged = true;
 	D3DXMATRIX mat;
	mat = matrixLookDirection(mpos,point-mpos);
 	D3DXQuaternionRotationMatrix(&mori,&mat);
}

void HDXCamera::setLookDirection(const D3DXVECTOR3 &dir) {
	setLookAt(mpos+dir);
}

D3DXMATRIX HDXCamera::getViewMatrix() const {
	if(mchanged) {
		//const...
		*((bool*)&mchanged) = false;

 		D3DXMATRIX mat_rot,mat_trans;
 		D3DXMatrixRotationQuaternion(&mat_rot,&mori);
 		D3DXMatrixTranslation(&mat_trans,VEC3TOARG(-mpos));
 
 		*(D3DXMATRIX*)&mview = mat_trans*mat_rot;
	}
	return mview;
}
