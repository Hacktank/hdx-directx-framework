
#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <directxmath.h>

struct HDXVERTPCTN {
	D3DXVECTOR3 p;
	D3DCOLOR c;
	D3DXVECTOR2 t;
	D3DXVECTOR3 n;

	static IDirect3DVertexDeclaration9 *decl;

	HDXVERTPCTN(const D3DXVECTOR3 &pp,
			   const D3DCOLOR &pc,
			   const D3DXVECTOR2 &pt,
			   const D3DXVECTOR3 &pn);
};
