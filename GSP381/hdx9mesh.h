#pragma once

#include <d3d9.h>
#include <d3dx9.h>

#include <string>
#include <vector>
#include <unordered_map>

#include "hdx9material.h"

class HDXTexture;

class HDXMesh {
	//creates a HDXMesh from an .x file, the HDXMesh's life can be managed, meaning only one instance of it can exist
	//and subsequent calls to the load function with same parameters will return the same instance
	friend HDXMesh* HDXMeshLoadFromX(const std::string &fname,const std::string &texturepath,bool managed);

	//creates a blank HDXMesh with the specified name, the HDXMesh's life can be managed, meaning only one instance of it can exist
	//and subsequent calls to the load function with same parameters will return the same instance
	friend HDXMesh* HDXMeshCreateBlank(const std::string &name,bool managed);

	friend HDXMesh* HDXMeshCreateSphere(float rad,const HDXMaterial &mat);
	friend HDXMesh* HDXMeshCreateBox(const D3DXVECTOR3 &ext,const HDXMaterial &mat);

public:
	ID3DXMesh *md3dmesh;
	std::vector<HDXMaterial> mmaterials;
	std::vector<HDXTexture*> mtextures;

	~HDXMesh();

	std::string getName() const;

	int release();

private:
	std::string mname;
	int musers;
	bool mmanaged;

	static std::unordered_map<std::string,HDXMesh*> __managed_items;

	HDXMesh();

	static HDXMesh* __create(const std::string &name,bool managed);
};
