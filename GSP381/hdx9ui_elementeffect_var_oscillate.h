#pragma once

#pragma warning(disable:4250)

#include "hdx9ui_elementeffect_transition_base.h"
#include "hdx9ui_elementeffect_varcontrolbase.h"

template<typename T>
class HDXUIElementEffect_Var_Oscillate : public HDXUIElementEffect_VarControlBase<T> {
public:
	HDXUIElementEffect_Var_Oscillate(const std::string &name,
									 HDXUIElementBase *parent,
									 std::function<T(void)> func_varget,
									 std::function<void(const T &var)> func_varset,
									 const T &wavelength,
									 float frequency,
									 float phaseshift = 0);
	virtual ~HDXUIElementEffect_Var_Oscillate();

	T getWavelength() const;
	void setWavelength(const T &var);

	float getFrequency() const;
	void setFrequency(float frequency);

	float getFrquencyCounter() const;

	virtual void update(float dt);

protected:
	virtual T _getEffect() const;
	virtual void _onDelta(const T &delta);

private:
	std::function<T(void)> mfunc_varget;
	std::function<void(const T &var)> mfunc_varset;
	T mwavelength;
	float mfrequency;
	float mfrequency_counter;
};

template<typename T>
HDXUIElementEffect_Var_Oscillate<T>::HDXUIElementEffect_Var_Oscillate(const std::string &name,
																	  HDXUIElementBase *parent,
																	  std::function<T(void)> func_varget,
																	  std::function<void(const T &var)> func_varset,
																	  const T &wavelength,
																	  float frequency,
																	  float phaseshift) :
																	  HDXUIElementEffect_VarControlBase(name,parent,func_varget,func_varset),
																	  HDXUIElementEffect_Base(name,parent) {
	mwavelength = wavelength;
	mfrequency = frequency;
	mfrequency_counter = phaseshift*mfrequency;
}

template<typename T>
HDXUIElementEffect_Var_Oscillate<T>::~HDXUIElementEffect_Var_Oscillate() {
}

template<typename T>
T HDXUIElementEffect_Var_Oscillate<T>::getWavelength() const {
	return mwavelength;
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::setWavelength(const T &var) {
	mwavelength = var;
}

template<typename T>
float HDXUIElementEffect_Var_Oscillate<T>::getFrequency() const {
	return mfrequency;
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::setFrequency(float frequency) {
	mfrequency = frequency;
}

template<typename T>
float HDXUIElementEffect_Var_Oscillate<T>::getFrquencyCounter() const {
	return mfrequency_counter;
}

template<typename T>
T HDXUIElementEffect_Var_Oscillate<T>::_getEffect() const {
	float s = sin((mfrequency_counter/mfrequency*3.14159f*2));
	return mwavelength*s;
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::_onDelta(const T &delta) {
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::update(float dt) {
	HDXUIElementEffect_VarControlBase<T>::update(dt);
	mfrequency_counter += dt;
	while(mfrequency_counter > mfrequency) {
		mfrequency_counter -= mfrequency;
	}
}
